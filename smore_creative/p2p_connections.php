<?php

/**
 * Smore Creative Posts 2 Posts Connection Types
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */

//----------- 

function my_connection_types() {
	p2p_register_connection_type( array( 
	'name' => 'work_for_client',
	'from' => 'work',
	'to' => 'clients',
	'reciprocal' => true,
	//'admin_box' => 'to',
	'title' => array( 'from' => 'For Client', 'to' => 'Work done for Client' ),
	'admin_column' => 'from'
) );


	p2p_register_connection_type( array( 
	'name' => 'testimonial_of_work',
	'from' => 'testimonials',
	'to' => 'work',
	'reciprocal' => true,
	//'admin_box' => 'from',
	'title' => array( 'from' => 'Work Praised', 'to' => 'Testimonial' ),
	'admin_column' => 'from'
) );


	p2p_register_connection_type( array( 
	'name' => 'testimonial_from_client',
	'from' => 'testimonials',
	'to' => 'clients',
	'reciprocal' => true,
	//'admin_box' => 'from',
	'title' => array( 'from' => 'Client Giving Testimonial', 'to' => 'Testimonials from Client' ),
	'admin_column' => 'from'
) );


	p2p_register_connection_type( array( 
	'name' => 'work_by_camper',
	'from' => 'work',
	'to' => 'campers',
	'reciprocal' => true,
	//'admin_box' => 'from',
	'title' => array( 'from' => 'Props', 'to' => 'Work Done' ),
	'admin_column' => 'from'
) );





}

add_action( 'p2p_init', 'my_connection_types' );