<?php

/**
 * Smore Creative Custom Post Types
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */

//----------- 


//----------- PUN - quotes from a smorefound at the bottom of the footer 
  register_post_type( 'pun',
    array(
      'labels' => array(
        'name' => __( 'S\'more Puns' ), //this name will be used when will will call the investments in our theme
        'singular_name' => __( 'Pun' ),
		'add_new' => _x('Add New', 'Pun'),
		'add_new_item' => __('Add A New Pun'), //custom name to show up instead of Add New Post. Same for the following
		'edit_item' => __('Edit Pun'),
		'new_item' => __('New Pun'),
		'view_item' => __('View Pun'),
      ),
      'public' => true,
	  'show_ui' => true,
	  'hierarchical' => true, //it means we cannot have parent and sub pages
	  'capability_type' => array( 'pun'),
	  'capabilities' => array(
				'publish_posts' => 'publish_pun',
				'edit_posts' => 'edit_pun',
				'delete_posts' => 'delete_pun',
				'read_private_posts' => 'read_private_pun',
				'edit_post' => 'edit_pun',
				'delete_post' => 'delete_pun',
				'read_post' => 'read_pun',
			),
	  'rewrite' => array('slug'=>'pun','with_front'=>false), //this is used for rewriting the permalinks
	  'query_var' => false,
	  'menu_icon' => ''.theme_url().'/assets/wp-pun.png',
	  'supports' => array( 'title'), //the editing regions that will support
    )
  );
  

//----------- WORK - A piece of work viewable in portfolio or blog 
  register_post_type( 'work',
    array(
      'labels' => array(
        'name' => __( 'Work' ), //this name will be used when will will call the investments in our theme
        'singular_name' => __( 'Work' ),
		'add_new' => _x('Add New', 'Work'),
		'add_new_item' => __('Add A New Piece of Work'), //custom name to show up instead of Add New Post. Same for the following
		'edit_item' => __('Edit Work'),
		'new_item' => __('New Work'),
		'view_item' => __('View Work'),
      ),
      'public' => true,
	  'has_archive' => true,
	  'show_ui' => true,
	  'hierarchical' => true, //it means we cannot have parent and sub pages
	  'capability_type' => array( 'work'),
	  'capabilities' => array(
				'publish_posts' => 'publish_work',
				'edit_posts' => 'edit_work',
				'delete_posts' => 'delete_work',
				'read_private_posts' => 'read_private_work',
				'edit_post' => 'edit_work',
				'delete_post' => 'delete_work',
				'read_post' => 'read_work',
				'upload_files' => 'upload_work',
			),
	  'rewrite' => array('slug'=>'work','with_front'=>false), //this is used for rewriting the permalinks
	  'query_var' => false,
	  'menu_icon' => ''.theme_url().'/assets/wp-work.png',
	  'supports' => array('title','thumbnail'), //the editing regions that will support
    )
  );
  
  
  

//----------- CLIENT - quotes from a smorefound at the bottom of the footer 
  register_post_type( 'clients',
    array(
      'labels' => array(
        'name' => __( 'Clients' ), //this name will be used when will will call the investments in our theme
        'singular_name' => __( 'Client' ),
		'add_new' => _x('Add New', 'Client'),
		'add_new_item' => __('Add New Client'), //custom name to show up instead of Add New Post. Same for the following
		'edit_item' => __('Edit Client'),
		'new_item' => __('New Client'),
		'view_item' => __('View Client'),
      ),
      'public' => true,
	  'show_ui' => true,
	  'hierarchical' => true, //it means we cannot have parent and sub pages
	  'capability_type' => array( 'client'),
	  'capabilities' => array(
				'publish_posts' => 'publish_client',
				'edit_posts' => 'edit_client',
				'delete_posts' => 'delete_client',
				'read_private_posts' => 'read_private_client',
				'edit_post' => 'edit_client',
				'delete_post' => 'delete_client',
				'read_post' => 'read_client',
			),
	  'rewrite' => array('slug'=>'clients','with_front'=>false), //this is used for rewriting the permalinks
	  'query_var' => false,
	  'menu_icon' => ''.theme_url().'/assets/wp-client.png',
	  'supports' => array('title','thumbnail'), //the editing regions that will support
    )
  );
  
  
  

//----------- TESTIMONIALS - quotes from a smorefound at the bottom of the footer 
  register_post_type( 'testimonials',
    array(
      'labels' => array(
        'name' => __( 'Testimonials' ), //this name will be used when will will call the investments in our theme
        'singular_name' => __( 'Testimonial' ),
		'add_new' => _x('Add New', 'Testimonial'),
		'add_new_item' => __('Add New Testimonial'), //custom name to show up instead of Add New Post. Same for the following
		'edit_item' => __('Edit Testimonial'),
		'new_item' => __('New Testimonial'),
		'view_item' => __('View Testimonial'),
      ),
      'public' => true,
	  'show_ui' => true,
	  'hierarchical' => true, //it means we cannot have parent and sub pages
	  'capability_type' => array( 'testimonial'),
	  'capabilities' => array(
				'publish_posts' => 'publish_testimonial',
				'edit_posts' => 'edit_testimonial',
				'delete_posts' => 'delete_testimonial',
				'read_private_posts' => 'read_private_testimonial',
				'edit_post' => 'edit_testimonial',
				'delete_post' => 'delete_testimonial',
				'read_post' => 'read_testimonial',
			),
	  'rewrite' => array('slug'=>'testimonials','with_front'=>false), //this is used for rewriting the permalinks
	  'query_var' => false,
	  'menu_icon' => ''.theme_url().'/assets/wp-testimonial.png',
	  'supports' => array('title'), //the editing regions that will support
    )
  );
  
  
  
  

//----------- CAMPERS - quotes from a smorefound at the bottom of the footer 
  register_post_type( 'campers',
    array(
      'labels' => array(
        'name' => __( 'Campers' ), //this name will be used when will will call the investments in our theme
        'singular_name' => __( 'Camper' ),
		'add_new' => _x('Add New', 'Camper'),
		'add_new_item' => __('Add New Camper'), //custom name to show up instead of Add New Post. Same for the following
		'edit_item' => __('Edit Camper'),
		'new_item' => __('New Camper'),
		'view_item' => __('View Camper'),
      ),
      'public' => true,
	  'show_ui' => true,
	  'hierarchical' => true, //it means we cannot have parent and sub pages
	  'capability_type' => array( 'camper'),
	  'capabilities' => array(
				'publish_posts' => 'publish_camper',
				'edit_posts' => 'edit_camper',
				'delete_posts' => 'delete_camper',
				'read_private_posts' => 'read_private_camper',
				'edit_post' => 'edit_camper',
				'delete_post' => 'delete_camper',
				'read_post' => 'read_camper',
			),
	  'rewrite' => 'campers', //this is used for rewriting the permalinks
	  'query_var' => false,
	  'menu_icon' => ''.theme_url().'/assets/wp-camper.png',
	  'supports' => array('title', 'thumbnail'), //the editing regions that will support ( title, editor, author, thumbnail, excerpt, trackbacks, custom-fields, comments, revisions, page-attributes, post-formats )
    )
  );
  
  
  add_action( 'init', 'register_taxonomies_and_types', 0 );
  
  
  // get the "author" role object
//$role = get_role( 'administrator' );
 
// add "organize_gallery" to this role object
//$role->add_cap( 'publish_camper' );
//				'(.*)' => '(.*)',
//$role->add_cap( '$2' );



$role = get_role( 'administrator' );

// $role->add_cap( 'publish_camper' );
// $role->add_cap( 'edit_camper' );
// $role->add_cap( 'delete_camper' );
// $role->add_cap( 'read_private_camper' );
// $role->add_cap( 'edit_camper' );
// $role->add_cap( 'delete_camper' );
// $role->add_cap( 'read_camper' );


// Add custom post types to archives
function custom_post_archive($query) {
if(!is_admin()) {
if (!is_post_type_archive() && $query->is_archive())
  $query->set( 'post_type', array('work', 'post') );
    remove_action( 'pre_get_posts', 'custom_post_archive' );
    }
}
add_action('pre_get_posts', 'custom_post_archive');
