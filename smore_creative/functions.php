<?php
include 'information.php';
include 'custom_post_types.php';
include 'shortcode.php';
include 'taxonomies.php';
include 'p2p_connections.php';
include 'reacurring_modles.php';


/**
 * Smore Creative functions and definitions
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Smore Creative 1.0
 */
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

if ( ! function_exists( 'smore_creative_setup' ) ):
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * @since Smore Creative 1.0
 */
function smore_creative_setup() {

	/**
	 * Custom template tags for this theme.
	 */
	require( get_template_directory() . '/inc/template-tags.php' );

	/**
	 * Custom functions that act independently of the theme templates
	 */
	//require( get_template_directory() . '/inc/tweaks.php' );

	/**
	 * Custom Theme Options
	 */
	//require( get_template_directory() . '/inc/theme-options/theme-options.php' );

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on Smore Creative, use a find and replace
	 * to change 'smore_creative' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'smore_creative', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'smore_creative' ),
	) );

	/**
	 * Add support for the Aside Post Formats
	 */
	add_theme_support( 'post-formats', array( 'aside', ) );
}
endif; // smore_creative_setup
add_action( 'after_setup_theme', 'smore_creative_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 *
 * @since Smore Creative 1.0
 */
function smore_creative_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Sidebar', 'smore_creative' ),
		'id' => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h1 class="widget-title">',
		'after_title' => '</h1>',
	) );
}
add_action( 'widgets_init', 'smore_creative_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function smore_creative_scripts() {
	wp_enqueue_style( 'style', get_stylesheet_uri() );

	wp_enqueue_script( 'modernizer', get_template_directory_uri() . '/js/modernizer.js', array( 'jquery' ), '20120204', true ); /* Modernizr*/

	wp_register_script( 'tinycarousel', get_template_directory_uri() . '/js/jquery.tinycarousel.min.js', array( 'jquery' ), '20120204', true ); /* Tiny Carousel  */

	wp_enqueue_script( 'fancyBox', get_template_directory_uri() . '/js/jquery.fancybox.pack.js', array( 'jquery' ), '20120204', true ); /* fancyBox */
	wp_enqueue_script( 'fancyBox-mousewheel', get_template_directory_uri() . '/js/jquery.mousewheel-3.0.6.pack.js', array( 'fancyBox' ), '20120204', true ); /* Add mousewheel plugin (this is optional)  */
	wp_enqueue_style( 'fancybox-style', get_template_directory_uri() .'/css/jquery.fancybox.css');

	wp_register_script( 'uniform', get_template_directory_uri() . '/js/jquery.uniform.min.js', array( 'jquery' ), '20120204', false ); /* Uniform Form Style  */
	wp_register_style( 'uniform-style', get_template_directory_uri() .'/css/uniform.smore.css');

	wp_enqueue_script( 'timeago', get_template_directory_uri() . '/js/timeago.js', array( 'jquery' ), '20120204', true ); /* Timeago (for twitter feed) */

	wp_enqueue_script( 'tooltips', get_template_directory_uri() . '/js/tooltips.min.js', array( 'jquery' ), '20120204', true ); /* Tool Tips  */

	wp_enqueue_script( 'smore', get_template_directory_uri() . '/js/smore.js', array( 'jquery' ), '20120204', true ); /* S'more   */

	wp_register_script( 'masonry', get_template_directory_uri() . '/js/masonry.js', array( 'jquery' ), '20130509', true ); /* S'more   */

	wp_register_script( 'quicksand', get_template_directory_uri() . '/js/jquery.quicksand.js', array( 'jquery' ), '20120204', false ); /* QuickSand  */
	wp_register_script( 'quicksand-plugins', get_template_directory_uri() . '/js/plugins.quicksand.js', array( 'jquery' ), '20120204', false ); /* QuickSand  */

 	wp_register_script( 'info-caroufredsel', get_template_directory_uri() . '/js/jquery.carouFredSel-5.5.0-packed.js', array('jquery'), '5.5.0', true );/*?*/
	wp_enqueue_script( 'small-menu', get_template_directory_uri() . '/js/small-menu.js', array( 'jquery' ), '20120206', true ); /*?*/
	
	if ( is_page('work') ) {
	    wp_enqueue_script('quicksand');
	    wp_enqueue_script('quicksand-plugins');
	    wp_enqueue_script('fancyBox');
	    wp_enqueue_script('fancyBox-mousewheel');
	    wp_enqueue_style('fancybox-style');
	  }

	if ( is_page('contact') ) {
	    wp_enqueue_script('uniform');
	    wp_enqueue_style('uniform-style');
	  }

	if ( is_page('home')) {
	    wp_enqueue_script('tinycarousel');
	    wp_enqueue_script('fancyBox');
	    wp_enqueue_script('fancyBox-mousewheel');
	    wp_enqueue_style('fancybox-style');
	  }

	if ( is_singular('work') ) {
	    wp_enqueue_script('fancyBox');
	    wp_enqueue_script('fancyBox-mousewheel');
	    wp_enqueue_style('fancybox-style');
	    wp_enqueue_script('tinycarousel');
	  }

	if ( is_page('about') ) {
	    wp_enqueue_script('fancyBox');
	    wp_enqueue_script('fancyBox-mousewheel');
	    wp_enqueue_style('fancybox-style');
	  }

	if ( is_page('blog') ) {
	    wp_enqueue_script('masonry');
	  }


	if ( is_archive() ) {
	    wp_enqueue_script('masonry');
	  }
 
 
}
add_action( 'wp_enqueue_scripts', 'smore_creative_scripts' );

/**
 * Implement the Custom Header feature
 */
//require( get_template_directory() . '/inc/custom-header.php' );


//WpAlchemy

include_once 'metaboxes/setup.php';

include_once 'metaboxes/spec.php';
 
//include_once 'metaboxes/full-spec.php';

//include_once 'metaboxes/checkbox-spec.php';

//include_once 'metaboxes/radio-spec.php';

//include_once 'metaboxes/select-spec.php';


function custom_register_terms() {
// First, set up the arguments for the WordPress function, get_posts().
	$args = array('post_type' => 'testimonials', 'numberposts', -1);
// Run get_posts() and attach it's output array to the variable, $testimonials.
	$testimonials = get_posts($args);
// Start an empty array which we will fill later.
	$lockdown_testimonials = array();
// Begin running through our array of testimonial posts with a foreach loop.

/* A foreach loop is a special method which allows us to take each part of 
an array and run the same group of functions over each one individually. */

// Treat each part of the $testimonials array as an individual piece called $testimonial.
	foreach ( $testimonials as $testimonial ) {
// Save each testimonial's title to a simpler array for comparison purposes later.
		$lockdown_testimonials[$testimonial->post_name] = $testimonial->post_title;
// Use WordPress' get_term_by() function to see if we've already created the association.
		if ( get_term_by('slug', $testimonial->post_name, 'association') == false ) {
// If not, create it using wp_insert_term().
			$insert = array('slug' => $testimonial->post_name);
			wp_insert_term( $testimonial->post_title, 'association', $insert );
		}
	}

/* Now that we're done creating any new terms, let's clean up the existing ones. */

// Get all of the existing association terms and set them to an array called $assocs.
	$assocs = get_terms('association', array('get' => 'all') );
// Run through the new array to see if we need to delete or update anything.
	foreach ( $assocs as $assoc ) {
		if ( $lockdown_testimonials[$assoc->slug] == '') {
// If the current term's post no longer exists, delete it using wp_delete_term().
			wp_delete_term($assoc->term_id, 'association');
		} elseif ( $assoc->name !== $lockdown_testimonials[$assoc->slug] ) {
// Otherwise, update it using wp_update_term(), just in case something has changed.
			$update = array(
				'name' => $lockdown_testimonials[$assoc->slug],
				'slug' => $assoc->slug
			);
			wp_update_term($assoc->term_id, 'association', $update);
		}
	}
}
// Hook the function into WordPress, run it every time something is done to a post.
add_action( 'save_post', 'custom_register_terms', 1);



// Add function to get slug
//http://www.tcbarrett.com/2011/09/wordpress-the_slug-get-post-slug-function/
// Get post slug
function the_slug() {
$post_data = get_post($post->ID, ARRAY_A);
$slug = $post_data['post_name'];
return $slug;
}

// Add new image size
if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'slideshow', 944, 700, false ); //(cropped)
    add_image_size( 'instagram', 68, 68, false ); //(cropped)
    add_image_size( 'instagram_blog', 252, 252, false ); //(cropped)
    add_image_size( 'client-featured', 160, 77, false ); //(cropped)
}
 
add_filter('image_size_names_choose', 'my_image_sizes');
function my_image_sizes($sizes) {
        $addsizes = array(
                "slideshow" => __( "Slideshow")
                );
        $newsizes = array_merge($sizes, $addsizes);
        return $newsizes;
}

// Adds featured image thumbnail to post backend 
add_action( 'right_now_content_table_end' , 'wph_right_now_content_table_end' );

if (function_exists( 'add_theme_support' )){
   add_filter('manage_posts_columns', 'posts_columns', 5);
   add_action('manage_posts_custom_column', 'posts_custom_columns', 5, 2);
   add_filter('manage_pages_columns', 'posts_columns', 5);
   add_action('manage_pages_custom_column', 'posts_custom_columns', 5, 2);
}
function posts_columns($defaults){
   $defaults['wps_post_thumbs'] = __('Thumbs');
   return $defaults;
}
function posts_custom_columns($column_name, $id){
	if($column_name === 'wps_post_thumbs'){
       echo the_post_thumbnail( array(125,80) );
   }
}

// http://alex.leonard.ie/2011/06/30/wordpress-check-if-post-is-in-custom-taxonomy/
/**
* Conditional function to check if post belongs to term in a custom taxonomy.
*
* @param    tax        string                taxonomy to which the term belons
* @param    term    int|string|array    attributes of shortcode
* @param    _post    int                    post id to be checked
* @return             BOOL                True if term is matched, false otherwise
*/
function smore_in_taxonomy($tax, $term, $_post = NULL) {
    // if neither tax nor term are specified, return false
    if ( !$tax || !$term ) { return FALSE; }
    // if post parameter is given, get it, otherwise use $GLOBALS to get post
    if ( $_post ) {
        $_post = get_post( $_post );
    } else {
        $_post =& $GLOBALS['post'];
    }
    // if no post return false
    if ( !$_post ) { return FALSE; }
    // check whether post matches term belongin to tax
    $return = is_object_in_term( $_post->ID, $tax, $term );
    // if error returned, then return false
    if ( is_wp_error( $return ) ) { return FALSE; }
    return $return;
}

// Adds wpAlchemy meta in RSS Feed
// http://www.wpbeginner.com/wp-tutorials/how-to-add-content-and-completely-manipulate-your-wordpress-rss-feeds/
function smore_wpalchemy_rss($content) {
global $about;
$about->the_meta();
$about->the_field('about');  
if(is_feed()) {
if ($about->have_value()) {
$content = $content. $about->get_the_value('about');
}
else {
$content = $content;
}
}
return $content;
}
add_filter('the_excerpt_rss', 'smore_wpalchemy_rss');
add_filter('the_content', 'smore_wpalchemy_rss');

// Change excerpt length
function custom_excerpt_length( $length ) {
return 500; // Change this to the number of characters you wish to have in your excerpt
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


// Remove wpautop filter
remove_filter( 'the_content', 'wpautop' );
add_filter( 'the_content', 'wpautop' , 99);