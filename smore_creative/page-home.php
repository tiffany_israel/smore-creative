<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */

get_header(); ?>

<!--[if lt IE 8]>
<script type="text/javascript">
jQuery(document).ready(function () {
alert("It looks like you're viewing our site in an older version of Internet Explorer. We reccommend using IE 8 and newer or other browsers such as Safari, Firefox or Google Chrome for your best viewing experience.");
});
</script>
<![endif]-->

		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
						<div class="entry-content" id="home">


<?php


$loop = new WP_Query( array( 'post_type' => 'work', 'posts_per_page' => 7, 'orderby' => 'menu_order', 'order' => 'ASC' , 'appearances' => 'home_slideshow' ) );
p2p_type( 'work_for_client' )->each_connected( $loop, array(), 'clients' );
p2p_type( 'testimonial_of_work' )->each_connected( $my_query, array(), 'testimonials' );
$size = 'xlarge';



     echo '<div id="smore_slideshow" class="home slideshow">';
     echo '<a href="#" class="buttons prev">&nbsp;</a><div class="viewport"><ul class="overview">';	
	if ($loop->have_posts()) :
			
			while ( $loop->have_posts() ) : $loop->the_post();
		$ID = $post->ID;
		echo "<li class='slide'>";
		
				//FLYER
				//if ($spokesimage->get_the_value('flyer')) {
				//while ($spokesimage->get_the_value('flyer')) {
				echo spokesimage_url('flyer', $size, $info, $sort);
				
				
				//} }
				
				//EMAIL
				//if ($spokesimage->get_the_value('email')) {
				echo spokesimage_url('email', $size, $info, $sort);
				
				//}
				
				//WEBSITE
				//if ($spokesimage->get_the_value('website')) {
				echo web_spokesimage_url('website', $size, $info, $sort);
				
				
				//}
				
				//POSTER
				//if ($spokesimage->get_the_value('poster')) {	
				echo spokesimage_url('poster', $size, $info, $sort);
				
				
				//}
				
				//SHIRT
				//if ($spokesimage->get_the_value('shirt')) {
				echo shirt_spokesimage_url('shirt', $size, $info, $sort);
				
				
				//}
				
				//PLAIN
				//if ($spokesimage->get_the_value('plain')) {	
				echo spokesimage_url('plain', $size, $info, $sort);
				
				
				//}
				
				//INVITATION
				//if ($spokesimage->get_the_value('invitation')) {
				echo invitation_spokesimage_url('invitation', $size, $info, $sort);
				
				
				//}
				
				//SERMON
				//if ($spokesimage->get_the_value('sermon')) {
				echo spokesimage_url('sermon', $size, $info, $sort);
				
				
				//}
				
			
				//BUSINESS CARD
				//if ($spokesimage->get_the_value('business_card')) {
				echo biz_spokesimage_url('business_card', $size, $info, $sort);

					echo '<div class="infobox">';

					global $about;
					global $subtitle;
		 
					// get the meta data for the current post
					$about->the_meta($ID);
					$subtitle->the_meta($ID);
					 
					// set current field, then get value
					$about->the_field('about');
					$subtitle->the_field('subtitle');
					$abouts = $about->get_the_value('about');

					/* SUBTITLE */
					echo '<h2><span class="small italic">';
					echo $subtitle->the_value();
					echo '</span>';
					echo "<br/>";

					/* CLIENT */
						if ( $post->clients ) {
						foreach ( $post->clients as $post ) : setup_postdata( $post );
						$client = $post->ID;

						echo get_the_title($client);
						echo '</h2>';
						echo "<br/>";
						// echo get_permalink($client);
						echo '<p><strong><a href="'.get_permalink($ID).'" title="'.get_the_title($ID).'">'.get_the_title($ID).'</a></strong>';

						endforeach;

					} else {
						echo get_the_title($ID);
						echo '</h2>';
						echo "<br/>";
						echo "<p>";
					}
					wp_reset_postdata();

					/* DESCRIPTION/ ABOUT*/


				if (strlen($abouts) > 400) $abouts = substr($abouts, 0, 97) . "...";
				echo '<br/>'.$abouts;

						echo "</p>";


					/* SKILLS */
					echo work_about_skills($ID);
					echo "<div class='divider blank'><br/></div>";

					/* TOOLs */
					echo work_about_tools($ID);


				
				
					
					foreach ( $post->testimonials as $post ) : setup_postdata( $post );
						
				$testimonial = $post->ID;
				echo '<p>testy ---> ';

				echo get_the_title($testimonial);
				echo '</p>';

					endforeach;

				echo "</div>"; // infobox
	wp_reset_postdata();


	                 
				
			echo "</li>";	
	endwhile; 
			
		echo '</ul>';
		echo '</div><!-- #viewport -->   ';
		echo '<a href="#" class="buttons next">&nbsp;</a>';
if ($loop->have_posts()) {

      
$counter = -1;
        echo '<ul class="pager">';
		while ( $loop->have_posts() ) : $loop->the_post();
$counter ++;
			
		echo '<li><a rel="';
		echo $counter;
		echo '" class="pagenum" href="#" title="';
		echo $final_attachments->the_value('title');
		echo '">&nbsp;</a></li>';
		endwhile;
		echo "</ul>";
		echo '<span class="pause"><a id="stopslider" class="button" href="#">hover to pause</a></span>';
		}
			endif;		
						
				
			echo '</div>';	/* #smore_slideshow */

?>

       <div class="row about clearfix" >
       		<h1 class="divider horizontal on_mallow"><span>About Us</span><a href="/about/"><span class="more on_mallow_dark">s'more about us</span></a></h1>
       			 <div class="column one separator vertical right">
       			 	<h2>Who we are and what we offer...</h2>
       			 	<br>
       			 	<p class="separator vertical left">We began <strong>S'more Creative</strong> because there's no time like the present to start offering people great graphic design and web services. We've got a surplus of creativity and an unrelenting passion to put it to great use for our clients.</p>

       			 	<ul class="iconography light horizontal paragraph">
       			 		<li class="location"><p>We are located in beautiful and occasional sunny Oxnard, California.</p></li>
       			 		<li class="nerd"><p>As a company we're just getting started but we're powered by an über nerd with loads of experience.</p></li>
       			 	</ul>

       			 </div><!-- .column.one -->
       			 <div>
	       			 <div class="column two">
	       			 	<!-- GRAPHIC DESIGN -->
	       			 	<?php echo about_graphic_design(); ?>

	       			 </div><!-- .column.two -->

	       			 <div class="column three">
	       			 	<!-- WEB DESIGN -->
	       			 	<?php echo about_web_design(); ?>

	       			 </div><!-- .column.three -->

	       			 <div class="column four">
	       			 	<!-- WEB DEVELOPMENT -->
	       			 	<?php echo about_web_development(); ?>
	       			 </div><!-- .column.four -->
       			</div>

	       			<div class="column five">
	       			 	<ul class="iconography light horizontal right">
	       			 		<li class="ask"><a href="#"><strong>We can't wait to five work with you!</strong> So what can we help you with?</a> </li>
	       			 	</ul> 
	       			</div>
       </div> 

        <div class="row testimonials slideshow clearfix" id="testimonials">
            <h1 class="divider horizontal on_mallow"><span>Testimonials</span><!-- <a href="#">Add s'more link when there is a testimonial page<span class="more on_mallow_dark">s'more testimonials</span></a> --></h1>
        	<div class="viewport"><ul class="overview" 	>
<?php
$my_query = new WP_Query( array( 'post_type' => 'testimonials', 'posts_per_page' => '5', 'orderby' => $orderby, 'order' => 'ASC') );

p2p_type( 'testimonial_from_client' )->each_connected( $my_query, array(), 'clients' );

while ( $my_query->have_posts() ) : $my_query->the_post();

global $testimonial;
				 
							$testimonial->the_meta();
							 
							$testimonial->the_field('quote');
				
				echo '<li class="shadow"><div class="testimonial on_orange shadow">';
					echo '<div class="quotation"></div>';
					echo '<p>"';
					$quote = $testimonial->get_the_value();
				if (strlen($quote) > 325) $quote = substr($quote, 0, 323) . "...";
				echo $quote;
					echo '"<span class="small">- ';
					echo $testimonial->the_value('name');
					echo '</span>';
					echo '</div></li>';


	wp_reset_postdata();
endwhile; ?>
</ul></div>

<?php $counter = -1; ?>

<ul class="pager">
<?php
$my_query = new WP_Query( array( 'post_type' => 'testimonials', 'posts_per_page' => '5', 'orderby' => $orderby, 'order' => 'ASC') );

p2p_type( 'testimonial_from_client' )->each_connected( $my_query, array(), 'clients' );

while ( $my_query->have_posts() ) : $my_query->the_post();
$counter ++;

		foreach ( $post->clients as $post ) :
	
global $post;
global $my_post_id;
// Save the post ID into the global variable
$my_post_id = $post->ID;
$the_slug = basename(get_permalink($my_post_id));

echo '<li><a rel="';
echo $counter;
echo '" class="pagenum" href="#">';
// When client pages exist uncomment
// echo '<a href="';
// echo site_url();
// echo '" title="';
// echo $the_slug;
// echo '" >';

echo '<img src="';
							echo theme_url();
							echo 'assets/clients/';
							echo $the_slug;
							echo '.png" alt="';
							echo $the_slug;
							echo '" />';
// echo "</a>";
echo '</a></li>';


	endforeach;

	wp_reset_postdata();

endwhile; ?>

</ul><!-- .pager -->


</div><!-- row -->


						</div><!-- .entry-content -->
					</article><!-- #post-<?php the_ID(); ?> -->


				<?php endwhile; // end of the loop. ?>

			</div><!-- #content .site-content -->
		</div><!-- #primary .content-area -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>