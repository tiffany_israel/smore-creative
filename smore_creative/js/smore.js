	


// Fix for IE not liking pseudo classes
    jQuery('div.row div.column:last-child').addClass('last');
    jQuery('.horizontal .spokesbox:last-child').addClass('last');
    jQuery('ul.iconography.horizontal li:last-child').addClass('last');
    jQuery('#instagram a:nth-child(3n+3)').addClass('last');
    jQuery('ul.pager li:last-child').addClass('last');

//Uniform
    jQuery(function(){
		jQuery("input[type='checkbox'], input[type='radio'], textarea, select").not("form#filter input[type='checkbox'], form#filter input[type='radio'], form#filter textarea, form#filter select").uniform();
    });

//Fancybox lightbox
	jQuery(document).ready(function() {
		jQuery("a.fancybox").fancybox();
	});

jQuery(document).ready(function() {
    jQuery(".iframe").fancybox({
        maxWidth    : 1200,
        maxHeight   : 5000,
        fitToView   : true,
        width       : '70%',
        height      : '70%',
        autoSize    : true,
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none'
    });
});

//Work Slideshow
    jQuery(document).ready(function(){
                
     jQuery('#smore_slideshow').tinycarousel({ pager: true, interval: true, intervaltime: 5000  });

    });


//Testimonial Slideshow
    jQuery(document).ready(function(){
                
        jQuery('#testimonials').tinycarousel({ pager: true, interval: true, intervaltime: 5000  });
        
    });



// // Tooltips
// jQuery(function() {
//     jQuery(".iconography.no_words a[title]").tooltips();
// });




