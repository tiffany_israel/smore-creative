<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */

get_header(); ?>

		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

<?php global $testimonial;
				 
							$testimonial->the_meta();
							 
							$testimonial->the_field('quote');
				
				echo '<div class="row testimonial on_orange shadow">';
					echo '<div class="quotation"></div>';
					echo '<p>"';
					echo $testimonial->the_value();
					echo '"<span class="small">- ';
					echo $testimonial->the_value('name');
					echo '</span>';
					echo '</div>';

					$connected = new WP_Query( array(
                      'connected_type' => 'testimonial_from_client',
                      'connected_items' => get_queried_object(),
                      'nopaging' => true,
                    ) );
					
					
                    // Display connected posts
                    if ( $connected->have_posts() ) :
                    
                    
                    
                    while ( $connected->have_posts() ) : $connected->the_post(); 
                echo 'howdy';
							echo the_title();
                endwhile;
					
					
                    // Prevent weirdness
                    wp_reset_postdata();
                    
                    endif;
					?>

			<?php endwhile; // end of the loop. ?>

			</div><!-- #content .site-content -->
		</div><!-- #primary .content-area -->

<?php get_footer(); ?>