<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */

get_header(); ?>

		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">


					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content" id="services">

					

					<!-- GRAPHIC DESIGN -->
					 <div class="row services clearfix first" id="graphic-design">

					 	<div class="column one separator vertical right">
					            	<div class="servicebox clearfix">
							            	<div class="about">
							            		<?php echo about_graphic_design(); ?>
							            	</div><!-- </div> .about -->
					            	</div> <!-- .servicebox -->
					            	<a href="/contact/" class="button rounded_corner clearfix" title="Let's get started">Let's get stared</a> 
					    </div> <!-- .column.one -->

					 	<div class="column two horizontal">
				            	<?php $size = 'mini' ?>
					            	<?php echo servicebox('service','print',$size,true); ?>
					            	<?php echo servicebox('service','logo',$size,true); ?>
					            	<?php echo servicebox('service','branding',$size,true, 1); ?>
					            	<?php echo servicebox('service','illustrations',$size,true); ?>
					            	<?php echo servicebox('service','sermon-graphic',$size,true); ?>
					            	<?php echo servicebox('service','t-shirts',$size,true); ?>
					            	<?php echo servicebox('service','invitations',$size,true); ?>
						    	
					    </div> <!-- .column.one -->

					</div> <!-- .row -->	
						<h1 class="divider horizontal on_mallow">&nbsp;</h1>

						<!-- WEB DESIGN -->
					 <div class="row services clearfix first" id="web-design">

					 	<div class="column one separator vertical right">
					            	<div class="servicebox clearfix">
							            	<div class="about">
							            		<?php echo about_web_design(); ?>
							            	</div><!-- </div> .about -->
					            	</div> <!-- .servicebox -->
					            	<a href="/contact/" class="button rounded_corner" title="Let's get started">Let's get stared</a> 
					    </div> <!-- .column.one -->

					 	<div class="column two horizontal">
				            	<?php $size = 'medium' ?>
					            	<?php echo servicebox('service','website',$size,true); ?>
					            	<?php echo servicebox('service','landing-page',$size,true); ?>
					            	<?php echo servicebox('service','html-email',$size,true); ?>
						    	
					    </div> <!-- .column.one -->

					</div> <!-- .row -->	

						<h1 class="divider horizontal on_mallow">&nbsp;</h1>

						<!-- WEB DEVELOPMENT -->
					 <div class="row services clearfix first" id="web-development">

					 	<div class="column one separator vertical right">
					            	<div class="servicebox">
							            	<div class="about">
							            		<?php echo about_web_development(); ?>
							            	</div><!-- </div> .about -->
					            	</div> <!-- .servicebox -->
					            	<a href="/contact/" class="button rounded_corner" title="Let's get started">Let's get stared</a> 
					    </div> <!-- .column.one -->

					 	<div class="column two horizontal">
				            	<ul class="iconography light horizontal paragraph">
				       			 		<li class="html5"><p><strong>HTML5</strong> Without geeking out about how awesome it is, we'll just say it's modern and we code sites with it.</p></li>
				       			 		<li class="css3"><p><strong>CSS3</strong> Still not BFFs with Internet Explorer, but we get along CSS3 great, and use it to fancify the sites we develop.</p></li>
				       			 		<li class="wordpress">
				       			 		  <p><strong>Wordpress</strong> It's not perfect, but we love it unconditionally. We build websites that our clients can easily update, thanks to the power of Wordpress</p></li>
				       			 		<li class="responsive"><p><strong>Responsive</strong> We develop equal opportunity opportunity websites, meaning they look just as spiffy on a small phone screen as they do on large desktop. </p></li>
				       			 	</ul>
						    	
					    </div> <!-- .column.one -->

					</div> <!-- .row -->	



					
						</div><!-- .entry-content -->
					</article><!-- #post-<?php the_ID(); ?> -->
			</div><!-- #content .site-content -->
		</div><!-- #primary .content-area -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>