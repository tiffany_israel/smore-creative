<?php
/**
 * @package Smore Creative
 * @since Smore Creative 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="divider">
			<span>
				<?php
				$category = get_the_category(); 
				echo $category[0]->cat_name;
				?>
			</span>
		</h1>
		<div class="clearfix" >
			<?php 
				 if ( has_post_thumbnail()) {
				   $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large'); ?>
					   <?php echo '<a class="fancybox photo shadow" data-fancybox-group="instagram" href="' . $large_image_url[0] . '" title="' . get_the_title() . '" >';
					   echo '<img src="'.site_url().'/wp-content/timthumb.php?src='.$large_image_url[0].'&amp;w=620"  alt="' . get_the_title() . '" />' ;
					   echo '</a>';
				 }
			?>	
			<ul class="iconography horizontal light no_words">
				<?php echo social_link('website'); ?>
				<?php echo social_link('facebook'); ?>
				<?php echo social_link('twitter'); ?>
				<?php echo social_link('dribbble'); ?>
				<?php echo social_link('forrst'); ?>
				<?php echo social_link('pinterest'); ?>
				<?php echo social_link('behance'); ?>
				<?php echo social_link('society6'); ?>
			</ul>
		</div>


		<div class="entry-meta row clearfix">
			<h2><?php the_title(); ?></h2>
			<p><?php wpautop(the_content()); ?></p>	
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->


<?php if (in_category('edible_smore')) : ?>
<!-- edible smore -->
<?php elseif (in_category('smore_love')) : ?>
<!-- smore love -->
<?php else : ?>
<!-- else -->
<?php endif; ?>

<!-- Ingredients -->
<div class="entry-content row clearfix">
	<?php global $recipe_ingredients;
		$recipe_ingredients->the_meta();
		$recipe_ingredients->the_field('recipe_ingredients');
		if($recipe_ingredients->have_value('recipe_ingredients')): ?>
			<div class="column one">
				<h1 class="divider"><span>Ingredients</span></h1>
					<ul class="list">
						<?php echo $recipe_ingredients->the_value(); ?>

					</ul>
			</div>

	<?php endif; ?>

	<!-- Facts -->
	<?php global $recipe_facts;
		$recipe_facts->the_meta();
		$recipe_facts->the_field('recipe_facts');
		if($recipe_facts->have_value('recipe_facts')): ?>
		<div class="column two">
			<h1 class="divider"><span>Facts</span></h1>
			<ul>
				<?php echo $recipe_facts->the_value(); ?>
			</ul>
		</div>
	<?php endif; ?>
				
</div>

<!-- Directions -->
<?php global $recipe_directions;
	$recipe_directions->the_meta();
	$recipe_directions->the_field('recipe_directions');
	if($recipe_directions->have_value('recipe_directions')): ?>
	<div class="row clearfix">
		<div class="column">
			<h1 class="divider"><span>Directions</span></h1>
				<ul class="list">
		<?php echo $recipe_directions->the_value(); ?>
				</ul>
			</div>
		</div>
<?php endif; ?>
	


        <!-- .column three: Process Photos if there are any -->
        <div class="row clearfix" id="process">
            <?php echo work_about_process(); ?>
        </div>

	<div class="entry-meta row">

				<h1 class="divider"><span>Post Info</span></h1>

		<?php
			/* translators: used between list items, there is a space after the comma */
			$category_list = get_the_category_list( __( ', ', 'smore_creative' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', ', ' );

			if ( ! smore_creative_categorized_blog() ) {
				// This blog only has 1 category so we just need to worry about tags in the meta text
				if ( '' != $tag_list ) {
					$meta_text = __( 'This entry was tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'smore_creative' );
				} else {
					$meta_text = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'smore_creative' );
				}

			} else {
				// But this blog has loads of categories so we should probably display them here
				if ( '' != $tag_list ) {
					$meta_text = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'smore_creative' );
				} else {
					$meta_text = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'smore_creative' );
				}

			} // end check for categories on this blog

			printf(
				$meta_text,
				$category_list,
				$tag_list,
				get_permalink(),
				the_title_attribute( 'echo=0' )
			);
		?>

		<?php edit_post_link( __( 'Edit', 'smore_creative' ), '<span class="edit-link">', '</span>' ); ?>
	
		<div class="row">
			<div class="column full"><?php smore_creative_content_nav( 'nav-below' ); ?></div>
		</div>

	</div><!-- .entry-meta -->
</article><!-- #post-<?php the_ID(); ?> -->
