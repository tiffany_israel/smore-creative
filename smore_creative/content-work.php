<?php
/**
 *
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="entry-content">




            <!-- .column three: Process Photos if there are any -->
                <?php echo work_slideshow(); ?>


        <div class="row work about clearfix">
<?php
global $process_photos;
$meta = $process_photos->the_meta(); ?>

            <?php if($process_photos->have_value('docs')): ?>
                <div class="column one ">
            <?php else: ?>
                <div class="column one " style="width:620px;">
            <?php endif; ?>
                <h1 class="divider horizontal on_mallow"><span>About this project</span></h1>

                <?php echo work_about_subtitle( 'work_for_client' ); ?>

            </div><!-- .column.one -->


            <div class="column two">


            <!-- SKILLS if ther are any -->
                <?php echo work_about_skills(); ?>

            <!-- TOOLS if ther are any -->
                <?php echo work_about_tools(); ?>

            </div><!-- .column.two -->


            <!-- .column three: Process Photos if there are any -->
                <?php echo work_about_process(); ?>


        </div><!-- .row.work.about -->

        <!-- Testimonial -->
        <?php echo work_testimonial( 'testimonial_of_work' ); ?>



        <?php echo work_props( 'work_by_camper' ); ?>




<?php $connected = new WP_Query( array(

                      'connected_type' => 'work_for_client',
                      'connected_items' => get_queried_object(),
                      'nopaging' => true,
                    ) );

?>
                    

                    <?php // Display connected posts
                    if ( $connected->have_posts() ) : ?>
                    
                    
                    <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
                    
                    
        <?php $related = p2p_type('work_for_client')->get_related( get_queried_object_id() );
            // Display related posts
            if ( $related->have_posts() ) :
                echo '<div class="row work client clearfix">';
            else: 
                echo '<div class="row work client no_other clearfix">';
                endif;
            ?>


        <?php echo work_client( 'work_for_client' ); ?>


        </div><!-- .row.work.client -->
                    
                    
                    <?php endwhile; ?>
                    
                    
                   <?php // Prevent weirdness
                    wp_reset_postdata();
                    
                    endif; ?>
                    




        <div class="row work similar clearfix">

        <div class="column one similar horizontal">
            <h1 class="divider horizontal on_mallow"><span>Similar Work</span><a href="<?php echo site_url();?>/work"><span class="more on_mallow_dark">s'more work</span></a></h1>
                <?php $terms = get_the_terms( $post->ID , 'service' );
if ( $terms ) {
    $counter = 0;
    foreach ( $terms as $term ) {
        $counter++;
        if ( $counter == 1 ) {

            $service = $term->slug;
        }
        else {
        }
    }
}

?>
                <?php echo similar_work( 'medium', false, 5, 'rand', $service ); ?>

            </div><!-- .column,two -->

        </div><!-- .row.work.client -->




    </div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
