<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */

get_header(); ?>

		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="entry-content">
		

<form id="filter" autocomplete="off">
  <fieldset class="service">
    <label class="rounded_corner button active"><input type="radio" name="type" value="all" checked="checked">All</label>
    <label class="rounded_corner button"><input type="radio" name="type" value="graphic-design">Graphic Design</label>
    <label class="rounded_corner button"><input type="radio" name="type" value="web-design">Web Design</label>
    <label class="rounded_corner button"><input type="radio" name="type" value="web-development">Web Development</label>
  </fieldset>
  <!-- <fieldset>
    <label style="border: solid 2px #000; display: block; float: left;"><input type="radio" name="service" value="graphic-design">Graphics</label>
    <label style="border: solid 2px #000; display: block; float: left;"><input type="radio" name="service" value="web-design">Web</label></fieldset> -->
  <fieldset class="sort">
    <label class="rounded_corner button active"><input type="radio"  name="sort" checked="checked">Newer</label>
    <label class="rounded_corner button"><input type="radio" name="sort" value="size" >Older</label>
    <label class="rounded_corner button"><input type="radio" name="sort" value="name">By Name</label>      
  </fieldset> 
</form>

<ul id="applications" class="image-grid">
  <!-- 
    <li data-id="id-13" data-type="graphic-design" data-service="graphic-design">
    <img src="http://razorjack.net/quicksand/content/images/textedit.png" width="128" height="128" alt="" />
    <strong>Graphic Design</strong> 
    <span data-type="size">1669 KB</span>
  </li> -->
                <?php echo work_sort( 'large', false, -1, '', '', true ); ?> 
</ul>


<!-- Quicksand -->

<script type="text/javascript">
// Custom sorting plugin
(function($) {
  $.fn.sorted = function(customOptions) {
    var options = {
      reversed: false,
      by: function(a) { return a.text(); }
    };
    $.extend(options, customOptions);
    $data = $(this);
    arr = $data.get();
    arr.sort(function(a, b) {
      var valA = options.by($(a));
      var valB = options.by($(b));
      if (options.reversed) {
        return (valA < valB) ? 1 : (valA > valB) ? -1 : 0;				
      } else {		
        return (valA < valB) ? -1 : (valA > valB) ? 1 : 0;	
      }
    });
    return $(arr);
  };
})(jQuery);

// DOMContentLoaded
$(function() {

  // bind radiobuttons in the form
  var $filterType = $('#filter input[name="type"]');
  var $filterSort = $('#filter input[name="sort"]');

  // get the first collection
  var $applications = $('#applications');

  // clone applications to get a second collection
  var $data = $applications.clone();

  // attempt to call Quicksand on every form change
  $filterType.add($filterSort).change(function(e) {
    if ($($filterType+':checked').val() == 'all') {
      var $filteredData = $data.find('> li');

    } else {
      // added ~ to following line to allow multiple categories ---> https://github.com/razorjack/quicksand/issues/21
      var $filteredData = $data.find('li[data-type~=' + $($filterType+":checked").val() + ']');
      // var $filteredData = $data.find('li[data-type~=' + $filterType + ']');
    }


    // if sorted by size
    if ($('#filter input[name="sort"]:checked').val() == "size") {
      var $sortedData = $filteredData.sorted({
        by: function(v) {
          return parseFloat($(v).find('span[data-type=size]').text());
        }
      });
    } else {
      // if sorted by name
      var $sortedData = $filteredData.sorted({
        by: function(v) {
          return $(v).find('span.mightier').text().toLowerCase();
        }
      });
    }   

    // finally, call quicksand
    $applications.quicksand($sortedData, {
      duration: 800,
      easing: 'easeInOutQuad'
    });

  });

});
</script>		




<!--  
 <script type="text/javascript">

// $("ul.image-grid > li").hover(function() {

// $(this).find(".hoverbox").css("display","block");
// },

// function () {
// $(this).find(".hoverbox").css("display","block");
//   }
// ); 
// </script>-->



<script type="text/javascript">

jQuery("ul.image-grid > li").live("mouseover", function() {

jQuery(this).find(".hoverbox").css("display","block");
jQuery(this).find(".spokesbox").css("display","none");
});

jQuery("ul.image-grid > li").live("mouseout", function() {

jQuery(this).find(".hoverbox").css("display","none");
jQuery(this).find(".spokesbox").css("display","block");
});


</script>


<script type="text/javascript">

jQuery("fieldset.service > label ").live("click", function() {

jQuery("fieldset.service > label ").removeClass("active");
jQuery(this).addClass("active");
});


jQuery("fieldset.sort > label ").live("click", function() {

jQuery("fieldset.sort > label ").removeClass("active");
jQuery(this).addClass("active");
});




</script>


        
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->

				<?php endwhile; // end of the loop. ?>

			</div><!-- #content .site-content -->
		</div><!-- #primary .content-area -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>