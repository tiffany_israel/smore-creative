<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="entry-content">
		
        
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
