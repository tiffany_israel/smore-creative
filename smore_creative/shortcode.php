<?php

/**
 * Smore Creative SHORTCODES
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */

// COLUMN
function column_shortcode( $atts, $content = null ) {
   extract( shortcode_atts( array(
      'number' => 'one',
      'class' => '',
      ), $atts ) );
 
   return '<div class="column ' . esc_attr($number) . ' '. esc_attr($class) . '">' . do_shortcode($content) . '</div>';
}
add_shortcode( 'column', 'column_shortcode' );

// COLUMN
function row_shortcode( $atts, $content = null ) {
   extract( shortcode_atts( array(
      'class' => '',
      ), $atts ) );
 
   return '<div class="row clearfix'. esc_attr($class) . '">' . do_shortcode($content) . '</div>';
}
add_shortcode( 'row', 'row_shortcode' );

// DIVIDER
function divider_shortcode( $atts, $content = null ) {
   extract( shortcode_atts( array(
      'number' => 'one',
      ), $atts ) );
 
   return '<h1 class="divider"><span>' . do_shortcode($content) . '</span></h1>';
}
add_shortcode( 'divider', 'divider_shortcode' );