<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */

get_header(); ?>

		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">


					<!-- BLOG -->
					<div class="row about clearfix" id="blog">
					 	

					 	<div class="column one">
					 		<h2 class="divider"><span>Blog</span></h2>
							<ul class="masonry">
								<li id="sidebar" class="clearfix blogbox"><?php get_sidebar('blog'); ?></li>
<?php $args = array('post_type'=>array('post', 'work'), 'posts_per_page' => -1,  'orderby' => 'date' );
query_posts($args);

global $spokesimage;
global $about;
global $subtitle;
$meta = $spokesimage->the_meta();	
$spokesimage->the_field('flyer');	
$spokesimage->the_field('flyer_imgurl');
$spokesimage->the_field('poster');	
$spokesimage->the_field('poster_imgurl');
$spokesimage->the_field('email');	
$spokesimage->the_field('email_imgurl');
$spokesimage->the_field('plain');	
$spokesimage->the_field('plain_imgurl');
$spokesimage->the_field('website');	
$spokesimage->the_field('website_imgurl');
$spokesimage->the_field('sermon');	
$spokesimage->the_field('sermon_imgurl');
$spokesimage->the_field('invitation');	
$spokesimage->the_field('invitation_imgurl');
$spokesimage->the_field('shirt');	
$spokesimage->the_field('shirt_imgurl');
$spokesimage->the_field('business_card');	
$spokesimage->the_field('business_card_imgurl');
$about->the_meta();
$about->the_field('about');	
$subtitle->the_meta();
$subtitle->the_field('subtitle');
$aboutp = $about->get_the_value('about');


if ( have_posts() ) : while ( have_posts() ) : the_post();?>
	<li class="blogbox clearfix <?php if ( in_category( 'instagram' )) { echo 'instagram'; } 
										elseif (smore_in_taxonomy('appearances', array('portfolio', 'wip')))  { echo 'work shadow'; } 
										else { echo 'shadow'; } ?>">
		<!-- Instagram -->
	<?php 
		if ( in_category( 'instagram' )) { ?>
			<?php 
				 if ( has_post_thumbnail()) {
				   $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large'); ?>
					   <ul class="iconography horizontal transparent no_words">
					   		<li><a href="" class="instagram"></a></li>
					   </ul>
					   <?php echo '<a class="fancybox photo shadow" data-fancybox-group="instagram" href="' . $large_image_url[0] . '" title="' . get_the_content() . '" >';
					   the_post_thumbnail('instagram_blog');
					   echo '</a>';
				 }
			?>		
		<!-- Smore to love & Edible Smore -->
		<?php } elseif ( in_category( array( 'smore_love', 'edible_smore' , 'geekisms' ) )) { ?>
			<ul class="iconography horizontal transparent no_words">
					<li><a href="" class="
						<?php if ( in_category( 'smore_love' )) { ?>
							heart
					    <?php } elseif ( in_category( 'edible_smore' )) { ?>
					    	coffee 
					    <?php } elseif ( in_category( 'geekisms' )) { ?>
					    	nerd
					    <?php } ?>"> </a>
					</li>
				</ul>

				<?php 
				 if ( has_post_thumbnail()) {
				   $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large'); ?>
					   
					   <?php echo '<a class="" href="'.get_permalink().'" target="_blank" title="' . get_the_title() . '" >';
					   the_post_thumbnail('instagram_blog');
					   echo '</a>';
				 } ?>	
				<div class="info">
					<h2 class="mightier"><?php the_title(); ?></h2>
					<a class="more left clearfix" target="blank" href="<?php the_permalink(); ?>">
						<?php global $subtitle;
							$subtitle->the_meta();
							$subtitle->the_field('subtitle');
							if($subtitle->have_value('subtitle')): ?>
								<?php echo $subtitle->the_value(); ?>
							<?php else: ?>
								View
							<?php endif; ?>
					</a><br/>
					<p><span class="italic"><?php the_time('F j, Y'); ?></span><br/><br/><?php $content = get_the_content();
							if (strlen($content) > 150) $content = substr($content, 0, 147) . "...";
						echo $content; ?>
					</p>	
				</div>

			<!-- Complete Work & Work in Progress -->
			<?php } else { ?>
				<ul class="iconography horizontal transparent no_words">
					<li><a href="" class="
						<?php if (smore_in_taxonomy('appearances', 'portfolio')) { ?>
							breifcase
					    <?php } elseif (smore_in_taxonomy('appearances', 'wip')) { ?>
					    	wrench 
					    	<?php } ?>"> </a>
					</li>
				</ul>

				<?php echo work_blog( 'medium', false, -1, '', '', false ); ?>
					<div class="info">
					<h2 class="mightier"><?php the_title(); ?></h2>
<!-- data-fancybox-group="blog" -->
					<a href="<?php the_permalink(); ?>?preview_theme=default&amp;preview_css=smore_simple" title="<?php the_title(); ?>"  data-fancybox-type="iframe" class="more left clearfix iframe">View Work</a><br/>
					<span><span class="italic"><?php the_time('F j, Y'); ?></span><br/><br/><?php $content = get_the_content();
							if (strlen($content) > 150) $content = substr($content, 0, 147) . "...";
						echo $content; ?>
					</span>	
					<?php  
					$abouts = $about->get_the_value('about');
						if (strlen($abouts) > 150) $abouts = substr($abouts, 0, 97) . "...";
						echo '<p>'.$abouts.'</p>' ?>
					
					</div>
					<?php } ?>
	</li>
	<?php endwhile; 
		wp_reset_postdata();
	endif; ?>
	</ul>	
						</div> <!-- .column -->
					
					</div> <!-- .row -->	

			</div><!-- #content .site-content -->
		</div><!-- #primary .content-area -->

<?php //get_sidebar(); ?>


<script>

  jQuery(function(){
    
    jQuery('.masonry').masonry({
      	itemSelector: '.blogbox',
		cornerStampSelector: '#sidebar',
		isAnimated: true,
		// gutterWidth: '24',
		// columnWidth: '252'
  		// columnWidth: 276,
    });
    
  });
</script>

<?php get_footer(); ?>