<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<article id="post-0" class="post error404 not-found">
					<h2 ><?php _e( 'Error 404: Ohh the s\'more-manity! That page can&rsquo;t be found.', 'smore_creative' ); ?></h2>
				<div class="entry-content">
					
					<img src="<?php echo theme_url();?>/assets/404.jpg" alt="a 404 image of a flaming marshmallow" />
				</div><!-- .entry-content -->
			</article><!-- #post-0 .post .error404 .not-found -->

		</div><!-- #content .site-content -->
	</div><!-- #primary .content-area -->

<?php get_footer(); ?>