<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */

get_header(); ?>

		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">

					<!--  WHO WE ARE -->
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content" id="about">

					 <div class="row about clearfix first" id="who">
					            <div class="column one">
					          			<img src="<?php echo theme_url(); ?>assets/logomark-about.png" alt="Smore"  />
					          		

								</div><!-- .one -->
					            <div class="column two">

					            	<h2>Who we are...</h2>
					            	<br/>
					            	<p>We began S'more Creative because there's no time like the present to start offering people great graphic design and web services. We've got a surplus of creativity and an unrelenting passion to put it to great use for our clients.</p>
								</div><!-- .two -->
					            <div class="column three">

					            	<h2>What's with the name?</h2>
					            	<br/>
					            	<p>Because what's better than having an edible mascot? Secondly, we love s'mores, but who doesn't? Third, we love puns and the word s'more has limitless potential for punning.</p>
								</div><!-- .three -->
					</div> <!-- .row -->	


					<!-- WHERE WE SET UP CAMP -->
					<div class="row about clearfix" id="camp">
					 	<div class="column">
						<h1 class="divider horizontal on_mallow"><span>Where we set up camp</span></h1>
					            <div class="column one">

					          		<img src="<?php echo theme_url(); ?>assets/california.png" alt="Oxnard, CA"   />
								</div><!-- .one -->
								<div class="column two">
									<ul class="iconography light horizontal paragraph">
				       			 		<li class="location"><p>We are located in beautiful and occasional sunny Oxnard, California.</p></li>
				       			 		<li class="tent"><p>We operate out of a quaint and cozy tent (home office), mere mile(s) from the shores of Oxnard Beach.</p></li>
				       			 		<li class="coffee"><p>We like to frequent local coffee shops like <a href="http://www.elementcoffeelounge.com" title="Element Cofee in Camarillo, CA">Element Coffee</a> in Camarillo (They serve cereal!) and <a href="http://www.palermoventura.com" title="Palermo Coffee House in Ventura, CA">Palermo</a> in downtown Venture (They make whip cream from scratch!)</p></li>
				       			 	</ul>
								</div><!-- .two -->
								</div> <!-- .column -->
								<div class="column three" >
									<h1 class="divider horizontal on_mallow"><span>Insta-Graham</span></h1>
									<div id="instagram" > 
									<?php $my_query = new WP_Query( array( 'posts_per_page' => 9, 'orderby' => 'date','post_type' => 'post', 'category_name' => 'instagram' ) ); ?>
								
						              <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
									 <?php 
										 if ( has_post_thumbnail()) {
										   $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
										   echo '<a class="fancybox photo shadow" data-fancybox-group="instagram" href="' . $large_image_url[0] . '" title="' . get_the_content() . '" >';
										   the_post_thumbnail('instagram');
										   echo '</a>';
										 }
									?>

						<?php endwhile; ?>
								</div><!-- #instagram -->
								<a class="more" href="/blog/category/instagram/">s'more grahms</a>
								</div> <!-- .column -->
					</div> <!-- .row -->	


					<!-- SERVICES -->
					 <div class="row about clearfix" id="services">

						<h1 class="divider horizontal on_mallow"><span>Our Services</span><a href="<?php echo site_url(); ?>/services"><span class="more on_mallow_dark">s'more about our services</span></a></h1>
					 	<div class="column one separator vertical right">
					            	<div class="servicebox">
						            	<?php echo servicebox('service','graphic-design','medium'); ?>
							            	<div class="about">
							            		<?php echo about_graphic_design(); ?>
							            	</div><!-- </div> .about -->
					            	</div> <!-- .servicebox -->
					    </div> <!-- .column.one -->

					 	<div class="column two">
				            	<div class="servicebox web-design">
					            	<?php echo servicebox('service','web-design','medium'); ?>
						            	<div class="about">
						            		<?php echo about_web_design(); ?>
						            	</div><!-- </div> .about -->
								</div> <!-- .servicebox -->
				            	<div class="servicebox web-devolpment">
					            	 <?php echo servicebox('service','web-development','medium'); ?>
						            	<div class="about">
						            		<?php echo about_web_development(); ?>
						            	</div><!-- </div> .about -->
				            	</div> <!-- .servicebox -->
						    	<div class="column five">
				       			 	<ul class="iconography light horizontal right">
				       			 		<li class="ask"><strong>We can't wait to work with you!</strong> So what can we help you with? </li>
				       			 	</ul> 
	       						</div><!-- </div> -->
					    </div> <!-- .column.one -->

					</div> <!-- .row -->	

					 

					<!-- STYLE -->
	 				<div class="row about clearfix" id="style">
							<h1 class="divider horizontal on_mallow"><span>Our Style</span></h1>
					 	<div class="column one">
					 		<h2>Karla</h2>
					 		<p>by <a href="http://www.jonpinhorn.com" title="Johnathan Pinhorn">Johnathan Pinhorn</a> </p>
					 		<p><strong>abcdefghijklmnopqrstuvwxyz<br/>ABCDEFGHIJKLMNOPQRSTUVWXYZ</strong></p>
						</div> <!-- .one -->

					 	<div class="column two">
					 		<!-- MELLOW MALLOW -->
					 		<div class="colorbox mellow_mallow shadow column">
					 			<div>
						 			<span class="title">Mellow Mallow</span>
						 			<span class="cmyk">C8 M11 Y26 K0</span>
						 			<span class="rgb">R232 G217 B189</span>
					 			</div>
						 		<span class="color"></span>
					 				<ul class="scale clearfix">
					 					<li class="light">light</li>
					 					<li class="dark">dark</li>
					 				</ul>
					 		</div>

					 		<!-- GRAHMALLOW -->
					 		<div class="colorbox grahmallow shadow column">
					 			<div>
						 			<span class="title">Grahmallow</span>
						 			<span class="cmyk">C6 M2 Y25 K0</span>
						 			<span class="rgb">R232 G228 B166</span>
					 			</div>
						 		<span class="color"></span>
					 				<ul class="scale clearfix">
					 					<li class="light">light</li>
					 					<li class="dark">dark</li>
					 				</ul>
					 		</div>

					 		<!-- OOEY GUI ORANGE -->
					 		<div class="colorbox ooey shadow column">
					 			<div>
						 			<span class="title">Ooey GUI Orange</span>
						 			<span class="cmyk">C6 M56 Y95 B0</span>
						 			<span class="rgb">R232 G217 B189</span>
					 			</div>
						 		<span class="color"></span>
					 				<ul class="scale clearfix">
					 					<li class="light">light</li>
					 					<li class="dark">dark</li>
					 				</ul>
					 		</div>

					 		<!-- CHOCOLATE BROWN -->
					 		<div class="colorbox choc_brown shadow column">
					 			<div>
						 			<span class="title">Choc Brown</span>
						 			<span class="cmyk">C57 M73 Y74 K81</span>
						 			<span class="rgb">R38 G13 B02</span>
					 			</div>
						 		<span class="color"></span>
					 				<ul class="scale clearfix">
					 					<li class="light">light</li>
					 					<li class="dark">dark</li>
					 				</ul>
					 		</div>

						</div> <!-- .two -->
					</div> <!-- .row -->

					<!-- HEAD CAMPER -->
					 <div class="row about clearfix" id="camper">
						<h1 class="divider horizontal on_mallow"><span>Our Head Camper</span></h1>
					            <div class="column">
					            	<?php $my_query = new WP_Query( array( 'posts_per_page' => 1, 'orderby' => 'menu_order','post_type' => 'campers') ); ?>
								
						              <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
									

								<?php global $about;
									global $subtitle;

						 
									// get the meta data for the current post
									$about->the_meta();
									$subtitle->the_meta();
									 
									// set current field, then get value
									$about->the_field('about');
									$subtitle->the_field();
										$about_p = $about->get_the_value();
										
									echo '<div class="column one">';
									if (has_post_thumbnail( $post->ID ) ): 
										$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
										echo '<a href="';
										echo $image[0];
										echo '" class="fancybox" title="';
										echo the_title();
										echo '">';
										echo '<img src="';
										echo $image[0];
										echo '" class="photo shadow" alt="';
										echo the_title();
										echo '" />';
										 echo "</a>";
									endif;
										
										echo '</div>';
									
									echo '<div class="column two">';
									echo '<h2>';
											echo the_title();
										echo '</h2><span class="small italic">';
											echo $subtitle->the_value('subtitle');
											echo '</span>';
											echo '<ul class="iconography horizontal light no_words">';
											echo social_link('website');
											echo social_link('facebook');
											echo social_link('twitter');
											echo social_link('dribbble');
											echo social_link('linkedin');
											echo social_link('forrst');
											echo '</ul><br/><br/>';
										echo wpautop($about_p);
									echo '</div>';

					  			?>

						<?php endwhile; ?>
								</div>
							
					</div> <!-- .row -->


					<!-- MORE THAN YOU EVER WANTED TO KNOW... -->
					 <div class="row about clearfix" id="wiki">
						<h1 class="divider horizontal on_mallow"><span>More than you ever wanted to know about s'mores</span></h1>
					            <div class="column columnize">

						<p>A s’more (sometimes spelled smore) is a traditional nighttime campfire treat popular in the United States and Canada consisting of a roasted marshmallow and a layer of chocolate sandwiched between two pieces of graham cracker.[1] National S’mores Day is celebrated yearly on August 10th in the United States.[2] 
						S’more appears to be a contraction of the phrase, "some more." While the origin of the dessert is unclear, the first recorded version of the recipe can be found in the publication "Tramping and Trailing with the Girl Scouts" of 1927.[3] The recipe is credited to Loretta Scott Crew, who reportedly made them by the campfire for the Scouts.[4] It is unknown whether the Girl Scouts were the first to make s’mores, but there appears to be no earlier claim to this snack. Although it is unknown when the name was shortened, recipes for "Some Mores" are in various Girl Scout publications until at least 1973.
						<br/>
						<a href="http://en.wikipedia.org/wiki/S’more" title="More than you ever wanted to know about smores" class="more left">http://en.wikipedia.org/wiki/S'more</a></p>
								</div>
					</div> <!-- .row -->
						</div><!-- .entry-content -->
					</article><!-- #post-<?php the_ID(); ?> -->



			</div><!-- #content .site-content -->
		</div><!-- #primary .content-area -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>