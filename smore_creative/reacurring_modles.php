<?php

/**
 * Smore Creative Recurring modles
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */


// Work Slideshow 
function work_slideshow() { 

global $final_attachments;
$meta = $final_attachments->the_meta();
$size = 'xlarge';
$spokes_slide = true;


      
     echo '<div id="smore_slideshow" class="work slideshow">';
     echo '<a href="#" class="buttons prev">&nbsp;</a>
	     	<div class="viewport">
	     		<ul class="overview">';

		echo "<li class='slide'>";
		
				//FLYER
				//if ($spokesimage->get_the_value('flyer')) {
				//while ($spokesimage->get_the_value('flyer')) {
				echo spokesimage_url('flyer', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
				
				
				//} }
				
				//EMAIL
				//if ($spokesimage->get_the_value('email')) {
				echo spokesimage_url('email', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
				
				//}
				
				//WEBSITE
				//if ($spokesimage->get_the_value('website')) {
				echo web_spokesimage_url('website', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
				
				
				//}
				
				//POSTER
				//if ($spokesimage->get_the_value('poster')) {	
				echo spokesimage_url('poster', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
				
				
				//}
				
				//SHIRT
				//if ($spokesimage->get_the_value('shirt')) {
				echo shirt_spokesimage_url('shirt', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
				
				
				//}
				
				//PLAIN
				//if ($spokesimage->get_the_value('plain')) {	
				echo spokesimage_url('plain', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
				
				
				//}
				
				//INVITATION
				//if ($spokesimage->get_the_value('invitation')) {
				echo invitation_spokesimage_url('invitation', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
				
				
				//}
				
				//SERMON
				//if ($spokesimage->get_the_value('sermon')) {
				echo spokesimage_url('sermon', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
				
				
				//}
				
			
				//BUSINESS CARD
				//if ($spokesimage->get_the_value('business_card')) {
				echo biz_spokesimage_url('business_card', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
 
				
			echo "</li>";	

if($final_attachments->have_value('docs')):
		while( $final_attachments->have_fields('docs') ) :
 
        echo '<li class="slide">';
        echo '<a href="';
		echo $final_attachments->the_value('attachurl');
		echo '" title="';
		echo $final_attachments->the_value('title');
		echo '" class="fancybox" data-fancybox-group="slideshow" ><img class="shadow photo" src="';
		echo site_url();
		echo '/wp-content/timthumb.php?src=';
		echo $final_attachments->the_value('attachurl');
		echo '&amp;w=944" alt="';
		echo $final_attachments->the_value('title');
		echo '"/></a></li>';
		
endwhile;

endif; 
		echo '</ul>';
		echo '<!-- .viewport --> </div>  ';

		echo '<a href="#" class="buttons next">&nbsp;</a>';

if($final_attachments->have_value('docs')):

      
$counter = 0;
        echo '<ul class="pager">';
		echo '<li><a href="#" class="pagenum" rel="0" >&nbsp;</a></li>'; //rel changed from data-slide, because it didn't work
		while( $final_attachments->have_fields('docs') ) :
$counter ++;
			
		echo '<li><a rel="';
		echo $counter;
		echo '" class="pagenum" href="#" title="';
		echo $final_attachments->the_value('title');
		echo '">&nbsp;</a></li>';
		
        
	

endwhile;
		echo '</ul>';
		echo '<ul class="iconography horizontal light no_words">';
				echo moonlight_link('website');
				echo moonlight_link('dribbble');
				echo moonlight_link('forrst');
				echo moonlight_link('behance');
				echo moonlight_link('pinterest');
				echo moonlight_link('society6');
		echo '</ul>';
endif; 

		echo '<!-- #work_slideshow --> </div>  ';
}




         
    
 
// The subtitle (a website for), the client (Sojourn Church), the about statement
function work_about_subtitle($connect_type) { 
				global $about;
				global $subtitle;

				global $process_photos;
	 
				// get the meta data for the current post
				$about->the_meta();
				$subtitle->the_meta();
				 
				// set current field, then get value
				$about->the_field('about');
				$subtitle->the_field('subtitle');
				$aboutp = $about->get_the_value('about');
				$meta = $process_photos->the_meta();
				
				
                    // Find connected posts
                    $connected = new WP_Query( array(
                      'connected_type' => $connect_type,
                      'connected_items' => get_queried_object(),
                      'nopaging' => true,
                    ) );
                    

                    // Display connected posts
                    if ( $connected->have_posts() ) :
                    
                    
                    while ( $connected->have_posts() ) : $connected->the_post(); 
					echo '<h2>';
					echo '<span class="small italic">';
					echo $subtitle->the_value();
					echo '</span>';
                    echo '<br/>';

					echo '<a href="';
					echo the_permalink();
					echo '">';
					echo the_title();
					echo '</a>';

					
					endwhile;
					
					else:

					echo '<h2>';
					echo '<span class="small italic">';
					echo $subtitle->the_value();
					echo '</span>';
                    echo '<br/>';
					echo the_title();


                    
                    endif;


					echo '</h2>';
					echo wpautop($aboutp);
					
                    // Prevent weirdness
                    wp_reset_postdata();
					
				
		
}


// List of Skills related to piece of work
function work_about_skills($ID) {


if ($ID) {

	$terms = get_the_terms( $ID, 'skill' );

	} else {

	$terms = get_the_terms( $post->ID, 'skill' );

	}		

if ( $terms && ! is_wp_error( $terms ) ) : 

	$skills = array();

	if ($ID) {

		echo '<div class="row">';
		echo '<ul class="skills rounded_corner">';

		} else {
		
		echo '<div class="row">';
		echo '<h1 class="divider horizontal on_mallow"><span>Skills</span></h1>';
		echo '<ul class="skills rounded_corner clearfix">';

		}		

	foreach ( $terms as $term ) {
		$skills[] = '<li>'.$term->name.'</li>';
	}
						
		$list_skills = join( "", $skills );


	echo $list_skills; 

	if ($ID) {

			echo '</ul>';
			echo '</div>';

			} else {
			
			echo '</ul>';
			echo '</div>';

			}	

endif;

			
}


// List of Skills related to piece of work
function work_about_tools($ID) {



if ($ID) {

	$terms = get_the_terms( $ID, 'tool' );

	} else {

	$terms = get_the_terms( $post->ID, 'tool' );

	}		

if ( $terms && ! is_wp_error( $terms ) ) : 

	$tools = array();

	if ($ID) {
		echo '<div class="row">';
		echo '<ul class="tools rounded_corner">';

		} else {
		echo '<div class="row">';
		echo '<h1 class="divider horizontal on_mallow"><span>tools</span></h1>';
		echo '<ul class="tools rounded_corner clearfix">';

		}		

	foreach ( $terms as $term ) {
		$tools[] = '<li class="'.$term->name.'">'.$term->slug.'</li>';
	}
						
		$list_tools = join( "", $tools );


	echo $list_tools; 

	if ($ID) {

			echo '</ul>';
			echo '</div>';

			} else {
			
			echo '</ul>';
			echo '</div>';

			}	

endif;

			
}





				 

 
// Process Photos 
function work_about_process() { 

global $process_photos;
$meta = $process_photos->the_meta();
	


if($process_photos->have_value('docs')):
      
     echo ' <div class="column three">';
     echo '<h1 class="divider horizontal on_mallow"><span>';

		if ( is_singular('work') ) {
			echo 'Process';
		} else {
			echo 'More Photos';
		}

	echo '</span></h1>';
    echo '<ul class="attachments photo">';
		while( $process_photos->have_fields('docs') ) :
 
        echo '<li>';
        echo '<a href="';
		echo $process_photos->the_value('attachurl');
		echo '" title="';
		echo $process_photos->the_value('title');
		echo '" class="fancybox" data-fancybox-group="process_photos" ><img class="shadow" src="';
		echo site_url();
		echo '/wp-content/timthumb.php?src=';
		echo $process_photos->the_value('attachurl');
		echo '&amp;w=68&amp;h=68" alt="';
		echo $process_photos->the_value('title');
		echo '"/></a></li>';
		
endwhile;
echo '</ul>';
echo '</div><!-- .column.three -->   ';
endif; 
}



// Testimonials
function work_testimonial($connect_type) { 
				
				
                    // Find connected posts
                    $connected = new WP_Query( array(
                      'connected_type' => $connect_type,
                      'connected_items' => get_queried_object(),
                      'nopaging' => true,
                    ) );
					
					
                    // Display connected posts
                    if ( $connected->have_posts() ) :
                    
                    
                    
                    while ( $connected->have_posts() ) : $connected->the_post(); 
					
						global $testimonial;
			 
						// get the meta data for the current post
						$testimonial->the_meta();
						 
						// set current field, then get value
						$testimonial->the_field('quote');
						
					echo '<div class="row work testimonial on_orange shadow clearfix">';
					echo '<div class="quotation"></div>';
					echo '<p>"';
					echo $testimonial->the_value();
					echo '"<span class="small">- ';
					echo $testimonial->the_value('name');
					echo '</span>';
					echo '</div>';
					
					
					
					endwhile;
					
					
                    // Prevent weirdness
                    wp_reset_postdata();
                    
                    endif;
	
}

function home_testimonials() { 

			



$loop = new WP_Query( array( 'post_type' => 'testimonials', 'posts_per_page' => '-1') );
	
			// Display related posts
			if ($loop->have_posts()) :

			p2p_type( 'testimonial_from_client' )->each_connected( $loop, array(), 'clients' );
			
			while ( $loop->have_posts() ) : $loop->the_post();
			global $testimonial;
				 
							// get the meta data for the current post
							$testimonial->the_meta();
							 
							// set current field, then get value
							$testimonial->the_field('quote');
				
				echo '<div class="row testimonial on_orange shadow">';
					echo '<div class="quotation"></div>';
					echo '<p>"';
					echo $testimonial->the_value();
					echo '"<span class="small">- ';
					echo $testimonial->the_value('name');
					echo '</span>';
					echo '</div>';

echo '<p>Client:</p>';

	foreach ( $post->clients as $post ) : setup_postdata( $post );
		echo 'howdy';
		the_title();

	endforeach;

	wp_reset_postdata();
	
				
	endwhile; 
			
			endif;		
						
					
}

// Props
function work_props($connect_type) { 
				
				
                    // Find connected posts
                    $connected = new WP_Query( array(
                      'connected_type' => $connect_type,
                      'connected_items' => get_queried_object(),
                      'nopaging' => true,
                    ) );
					
					
                    // Display connected posts
                    if ( $connected->have_posts() ) :
                    
                    echo '<div class="row work props clearfix">';
                    
                    while ( $connected->have_posts() ) : $connected->the_post(); 
					
						global $about;
						global $subtitle;

			 
						// get the meta data for the current post
						$about->the_meta();
						$subtitle->the_meta();
						 
						// set current field, then get value
						$about->the_field('about');
						$subtitle->the_field();
							$about_p = $about->get_the_value();
							
					 echo '<div class="column one">';
						echo '<h1 class="divider horizontal on_mallow"><span>Props</span></h1>';
						echo '<div class="column one">';
						if (has_post_thumbnail( $post->ID ) ): 
							$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
										echo '<a href="';
										echo $image[0];
										echo '" class="fancybox" title="';
										echo the_title();
										echo '">';
										echo '<img src="';
										echo $image[0];
										echo '" class="photo shadow" alt="';
										echo the_title();
										echo '" />';
										 echo "</a>";
						endif;
							echo '<h2>';
								echo the_title();
							echo '</h2><span class="small italic">';
								echo $subtitle->the_value('subtitle');
								echo '</span>';
								echo '<ul class="iconography horizontal light no_words">';
								echo social_link('website');
								echo social_link('facebook');
								echo social_link('twitter');
								echo social_link('dribbble');
								echo social_link('linkedin');
								echo social_link('forrst');
								echo '</ul>';
							echo '</div>';
						
						echo '<div class="column two">';
							echo wpautop($about_p);
						echo '</div>';
					 echo '</div>';
					 
					
					endwhile;

        				echo '<div class="column two info_right">';
                		echo other_work( 'work_by_camper', 'small', true, 4, 'divider horizontal on_mallow', 'Other Work' , 's\'more work'  );
           				echo '</div><!-- .column,two -->';
        				echo '</div><!-- .row.work.props -->';
					
					
                    // Prevent weirdness
                    wp_reset_postdata();
                    
                    endif;
					
}



// Props
function other_work($connected_type, $size, $info, $number, $h1_class, $divider_text, $more_text) { 

			

global $spokesimage;
$meta = $spokesimage->the_meta();	
$spokesimage->the_field('flyer');	
$spokesimage->the_field('flyer_imgurl');
$spokesimage->the_field('poster');	
$spokesimage->the_field('poster_imgurl');
$spokesimage->the_field('email');	
$spokesimage->the_field('email_imgurl');
$spokesimage->the_field('plain');	
$spokesimage->the_field('plain_imgurl');
$spokesimage->the_field('website');	
$spokesimage->the_field('website_imgurl');
$spokesimage->the_field('sermon');	
$spokesimage->the_field('sermon_imgurl');
$spokesimage->the_field('invitation');	
$spokesimage->the_field('invitation_imgurl');
$spokesimage->the_field('shirt');	
$spokesimage->the_field('shirt_imgurl');
$spokesimage->the_field('business_card');	
$spokesimage->the_field('business_card_imgurl');

	$related = p2p_type($connected_type)->get_related( get_queried_object_id() );
	$count=0;
			// Display related posts
			if ( $related->have_posts() ) :
			
			echo '<h1 class="'.$h1_class.'"><span>'.$divider_text.'</span><a href="';
			echo site_url();
			echo '/work"><span class="more on_mallow_dark">'.$more_text.'</span></a></h1>';
			while ( $related->have_posts() ) : $related->the_post();
			 $count++;	
			if ($count < $number)	 {


				//FLYER
				//if ($spokesimage->get_the_value('flyer')) {
				//while ($spokesimage->get_the_value('flyer')) {
				echo spokesimage_url('flyer', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
				
				
				//} }
				
				//EMAIL
				//if ($spokesimage->get_the_value('email')) {
				echo spokesimage_url('email', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
				
				//}
				
				//WEBSITE
				//if ($spokesimage->get_the_value('website')) {
				echo web_spokesimage_url('website', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
				
				
				//}
				
				//POSTER
				//if ($spokesimage->get_the_value('poster')) {	
				echo spokesimage_url('poster', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
				
				
				//}
				
				//SHIRT
				//if ($spokesimage->get_the_value('shirt')) {
				echo shirt_spokesimage_url('shirt', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
				
				
				//}
				
				//PLAIN
				//if ($spokesimage->get_the_value('plain')) {	
				echo spokesimage_url('plain', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
				
				
				//}
				
				//INVITATION
				//if ($spokesimage->get_the_value('invitation')) {
				echo invitation_spokesimage_url('invitation', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
				
				
				//}
				
				//SERMON
				//if ($spokesimage->get_the_value('sermon')) {
				echo spokesimage_url('sermon', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
				
				
				//}
				
			
				//BUSINESS CARD
				//if ($spokesimage->get_the_value('business_card')) {
				echo biz_spokesimage_url('business_card', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
				
				
			}
	endwhile; 
			
			else : 
			
				// Prevent weirdness
			wp_reset_postdata();

			endif;		
						
					
}




//Spokesimage info
function spokesimage_info($spokesimage_info, $size) { 

global $about;
global $spokesimage;
$meta = $spokesimage->the_meta();
$spokesimage->the_field($spokesimage_info);
$about->the_meta();
$about->the_field('about');	
	

		if ($spokesimage->get_the_value($spokesimage_url)) {
			echo '<p class="info ';
			echo $size;
			echo 'er">';
			echo '<span class="mightier">';
			$title = get_the_title();
				if (strlen($title) > 25) $title = substr($title, 0, 22) . "...";
				echo $title;
			echo '</span>';
			$abouts = $about->get_the_value('about');
				if (strlen($abouts) > 100) $abouts = substr($abouts, 0, 97) . "...";
				echo $abouts;
			echo '</p>';
		}

}

//Spokesimage img src
function spokesimage_url($spokesimage_url, $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog) { 

global $about;
global $spokesimage;
$meta = $spokesimage->the_meta();
$spokesimage->the_field($spokesimage_url);
$spokesimage->the_field($spokesimage_url.'_imgurl');
$about->the_meta();
$about->the_field('about');	

		if ($spokesimage->get_the_value($spokesimage_url)) {
			if ($sort == true) {
				
				echo '<li data-type="';
				echo get_service();
				echo '" data-subservice="';
				echo $spokesimage_url;
				echo '" data-id="">';
				echo hoverbox($size);
				
				} 
				
			echo '<div class="spokesbox">';

			if ($spokes_slide == false && $blog == false) {
			echo '<a href="'.get_permalink().'" title="'.get_the_title().'">';	
			}
			if ($blog == true)  {
				    echo '<a href="'.get_permalink().'?preview_theme=default&amp;preview_css=smore_simple" title="'.get_the_title().'"  data-fancybox-type="iframe" class="iframe">';
				  }	

			echo '<div class="spokesimage title ';
			echo $size;
			echo ' ">';
			echo '<img src="';
			echo site_url();
			echo '/wp-content/timthumb.php?src=';
			echo $spokesimage->the_value($spokesimage_url.'_imgurl');
			if ($size == 'xlarge') {
					echo '&amp;w=433"  alt="';
				} else {
					echo '&amp;w=433"  alt="';
				}
			echo the_title();
			echo '" class="shadow ';
			echo $spokesimage_url;
			echo '" />';
			echo '</div><!-- spokesimage -->';

			if ($info == true) {
				echo spokesimage_info($spokesimage_url, $size);
				} 

			if ($spokes_slide == false)  {
			echo '</a>';
			}

			if ($serviceinfo == true) {
					echo serviceinfo($service, $size);
				} 
			echo '<!-- spokesbox --></div>';
			if ($sort == true) {
				echo ' <span class="data-type-size" data-type="size">'.get_the_ID().'</span></li>';
				} else {
				}
		}
}


function get_service() { 

// $terms = get_the_terms( $post->ID , 'service' );
// 					$order_id = $family_id = $subfamily_id = 0;

// 						foreach ( $terms as $term ) {
// 				        if ( $order_id || $term->parent )
// 				            continue;
// 				        $order_id  = $term->term_id;
// 				        $service = $term->slug;


// 				    }
// 				    echo $service;



$terms = get_the_terms( $post->ID, 'service' );
						$order_id = $family_id = $subfamily_id = 0;
if ( $terms && ! is_wp_error( $terms ) ) : 

	$draught_links = array();

	foreach ( $terms as $term ) {
		if ( $order_id || $term->parent )
				            continue;
		$draught_links[] = $term->slug;
	}
						
	$on_draught = join( " ", $draught_links );

	// if ($on_draught == 'web-design_web-development') {
	// 	$on_draught = 'web-development';
	// }

echo $on_draught; 

 endif; 
}



//Spokesimage img src for business cards
function hoverbox($size) { 
	
global $about;
global $subtitle;
$about->the_meta();
$about->the_field('about');	
$subtitle->the_meta();
$subtitle->the_field('subtitle');
$aboutp = $about->get_the_value('about');
echo '<div class="hoverbox shadow">';
		echo '<a href="';
		echo the_permalink();
		echo '">';
						echo '<p class="info ';
			echo $size;
			echo 'er">';
			echo '<span class="mightier">';
			$title = get_the_title();
				if (strlen($title) > 30) $title = substr($title, 0, 22) . "...";
				echo $title;
			echo '</span>';
			$abouts = $about->get_the_value('about');
				if (strlen($abouts) > 150) $abouts = substr($abouts, 0, 97) . "...";
				echo $abouts;

	echo '<ul class="tools rounded_corner">';
	foreach(get_the_terms($post->ID, 'tool' ) as $term)
         echo '<li class="'.$term->name.'">'.$term->slug.'</li>';
	echo '</ul>'; 
				echo '</a></div>';



}








//Spokesimage img src for business cards
function biz_spokesimage_url($spokesimage_url, $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog) { 

 global $spokesimage;
$meta = $spokesimage->the_meta();
$spokesimage->the_field($spokesimage_url);
$spokesimage->the_field($spokesimage_url.'_front_imgurl');
$spokesimage->the_field($spokesimage_url.'_back_imgurl');
$spokesimage->the_field('biz_orientation');	
	

		if ($spokesimage->get_the_value($spokesimage_url)) {
			if ($sort == true) {
				
				echo '<li class="quicksand" data-type="';
				echo get_service();
				echo '"  data-id="">';
				echo hoverbox($size);
				
				} else {
				}
			echo '<div class="spokesbox">';
			
			if ($spokes_slide == false && $blog == false) {
			echo '<a href="'.get_permalink().'" title="'.get_the_title().'">';	
			}
			if ($blog == true)  {
				    echo '<a href="'.get_permalink().'?preview_theme=default&amp;preview_css=smore_simple" title="'.get_the_title().'"  data-fancybox-type="iframe" class="iframe">';
				  }	

			echo '<div class="spokesimage title business_card ';
			echo $spokesimage->the_value('biz_orientation');
			echo ' '.$size;
			echo ' ">';
			
			echo '<img src="';
			echo site_url();
			echo '/wp-content/timthumb.php?src=';
			echo $spokesimage->the_value($spokesimage_url.'_back_imgurl');
			if ($size == 'xlarge') {
					echo '&amp;w=433"  alt="';
				} else {
					echo '&amp;w=252"  alt="';
				}
			echo the_title();
			echo '" class="back shadow ';
			echo $spokesimage_url;
			echo '" />';
			echo '<img src="';
			echo site_url();
			echo '/wp-content/timthumb.php?src=';
			echo $spokesimage->the_value($spokesimage_url.'_front_imgurl');
			if ($size == 'xlarge') {
					echo '&amp;w=433"  alt="';
				} else {
					echo '&amp;w=252"  alt="';
				}
			echo the_title();
			echo '" class="front shadow ';
			echo $spokesimage_url;
			echo '" />';
			echo '</div>';
			if ($info == true) {
				echo spokesimage_info($spokesimage_url, $size);
				} 
			
			if ($spokes_slide == false) {
			echo '</a>';
			}
			if ($serviceinfo == true) {
					echo serviceinfo($service, $size);
				} 
			echo '</div>';
			if ($sort == true) {
				echo ' <span class="data-type-size" data-type="size">'.get_the_ID().'</span></li>';
				} else {
				}
		}

}


//Spokesimage img src for website
function web_spokesimage_url($spokesimage_url, $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog) { 

 global $spokesimage;
$meta = $spokesimage->the_meta();
$spokesimage->the_field($spokesimage_url);
	

		if ($spokesimage->get_the_value($spokesimage_url)) {
			if ($sort == true) {
				
				echo '<li class="quicksand" data-type="';
				echo get_service();
				echo '"  data-id="">';
				echo hoverbox($size);
				
				} else {
				}
			echo '<div class="spokesbox">';

			
			if ($spokes_slide == false && $blog == false) {
			echo '<a href="'.get_permalink().'" title="'.get_the_title().'">';	
			}
			if ($blog == true)  {
				    echo '<a href="'.get_permalink().'?preview_theme=default&amp;preview_css=smore_simple" title="'.get_the_title().'"  data-fancybox-type="iframe" class="iframe">';
				  }	


			echo '<div class="spokesimage title website ';
			echo $size;
			echo ' ">';
				if ($size == 'xlarge') {
						echo '<div class="computer ';
						echo $size;
						echo '">';
					} else {
						echo '<div class="browser shadow"></div>';
					}
			echo '<img src="';
			echo site_url();
			echo '/wp-content/timthumb.php?src=';
			echo $spokesimage->the_value($spokesimage_url.'_imgurl');
				if ($size == 'xlarge') {
						echo '&amp;w=433&amp;h=250&amp;a=t"  alt="';
					} else {
						echo '&amp;w=252"  alt="'; /* Don't add height attribute because it zooms in and crops off too much */
					}
			echo the_title();
			echo '" class=" shadow ';
			if ($size == 'xlarge') {
					echo "screen ";
				} else {
				}
			echo $spokesimage_url;
			echo '" />';
			if ($size == 'xlarge') {
                    echo '<img src="'.theme_url().'assets/spokesimage/web_mask_xlarge.png" alt="Website"  class="mask" />';
                    echo '</div>'; // .computer
				} else {
					echo '';
				}

			echo '</div>';
			if ($info == true) {
				echo spokesimage_info($spokesimage_url, $size);
				} 

			if ($spokes_slide == false) {
			echo '</a>';
			}

			if ($serviceinfo == true) {
					echo serviceinfo($service, $size);
				} 
			echo '</div>';
		}

}


//Spokesimage img src for website
function shirt_spokesimage_url($spokesimage_url, $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog) { 

 global $spokesimage;
$meta = $spokesimage->the_meta();
$spokesimage->the_field($spokesimage_url);
$spokesimage->the_field('shirt-color');
	

		if ($spokesimage->get_the_value($spokesimage_url)) {
			if ($sort == true) {
				
				echo '<li class="quicksand" data-type="';
				echo get_service();
				echo '"  data-id="">';
				echo hoverbox($size);
				
				} else {
				}
			echo '<div class="spokesbox">';
			
			
			if ($spokes_slide == false && $blog == false) {
			echo '<a href="'.get_permalink().'" title="'.get_the_title().'">';	
			}
			if ($blog == true)  {
				    echo '<a href="'.get_permalink().'?preview_theme=default&amp;preview_css=smore_simple" title="'.get_the_title().'"  data-fancybox-type="iframe" class="iframe">';
				  }	


			echo '<div class="spokesimage title shirt '.$size.'" style="background-color: #';
			echo $spokesimage->the_value('shirt-color');
			echo '">';
			echo '<img src="';
			echo site_url();
			echo '/wp-content/timthumb.php?src=';
			echo $spokesimage->the_value($spokesimage_url.'_imgurl');
			if ($size == 'xlarge') {
					echo '&amp;w=433"  alt="';
				} else {
					echo '&amp;w=252"  alt="';
				}
			echo the_title();
			echo '" class=" shadow ';
			echo $spokesimage_url;
			echo '" />';
			echo '</div>';
			if ($info == true) {
				echo spokesimage_info($spokesimage_url, $size);
				} 
			
			if ($spokes_slide == false) {
				echo '</a>';
			}

			if ($serviceinfo == true) {
					echo serviceinfo($service, $size);
				} 
			echo '</div>';
			if ($sort == true) {
				echo ' <span class="data-type-size" data-type="size">'.get_the_ID().'</span></li>';
				} else {
				}
		}

}


//Spokesimage img src for website
function invitation_spokesimage_url($spokesimage_url, $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog) { 

 global $spokesimage;
$meta = $spokesimage->the_meta();
$spokesimage->the_field($spokesimage_url);
$spokesimage->the_field('envelope-color');
	

		if ($spokesimage->get_the_value($spokesimage_url)) {
			if ($sort == true) {
				
				echo '<li data-type="';
				echo get_service();
				echo '"  data-id="13" >';
				echo hoverbox($size);
				
				} else {
				}
			echo '<div class="spokesbox">';

			
			if ($spokes_slide == false && $blog == false) {
			echo '<a href="'.get_permalink().'" title="'.get_the_title().'">';	
			}
			if ($blog == true)  {
				    echo '<a href="'.get_permalink().'?preview_theme=default&amp;preview_css=smore_simple" title="'.get_the_title().'"  data-fancybox-type="iframe" class="iframe">';
				  }	


			echo '<div class="spokesimage title invitation '.$size.'" style="'; 
			if (($size == 'xlarge') || ($size == 'large') || ($size == 'medium')) {
					echo 'background-color: #';
					echo $spokesimage->the_value('envelope-color');
				} else {
					echo 'background: none;';
				}
			echo '">';
			echo '<img src="';
			echo site_url();
			echo '/wp-content/timthumb.php?src=';
			echo $spokesimage->the_value($spokesimage_url.'_imgurl');
			if ($size == 'xlarge') {
					echo '&amp;w=433"  alt="';
				} else {
					echo '&amp;w=252"  alt="';
				}
			echo the_title();
			echo '" class=" shadow ';
			echo $spokesimage_url;
			echo '" />';
			echo '</div>';
			if ($info == true) {
				echo spokesimage_info($spokesimage_url, $size);
				} 

			if ($spokes_slide == false) {
				echo '</a>';
			}

			if ($serviceinfo == true) {
					echo serviceinfo($service, $size);
				} 
			echo '</div>';
			if ($sort == true) {
				echo ' <span class="data-type-size" data-type="size">'.get_the_ID().'</span></li>';
				} else {
				}
		}

}


// Props
function work_client($connect_type) { 
				
				
                    // Find connected posts
                    $connected = new WP_Query( array(
                      'connected_type' => $connect_type,
                      'connected_items' => get_queried_object(),
                      'nopaging' => true,
                    ) );
					
					
                    // Display connected posts
                    if ( $connected->have_posts() ) :
                    
                    
                    
                    while ( $connected->have_posts() ) : $connected->the_post(); 
					

			

							
					 echo '<div class="column one">';
						echo '<h1 class="divider horizontal on_mallow"><span>Client</span></h1>';
						echo '<div class="column one">';
						
						$filename = '/home5/tiffanyi/public_html/smorecreative/wp-content/themes/smore_creative/assets/clients/'.the_slug().'.png';

						if (file_exists($filename)) {
							echo '<img src="';
							echo theme_url();
							echo 'assets/clients/';
							echo the_slug();
							echo '.png" alt="';
							echo the_slug();
							echo '" />';
							echo '</div>';

						} else {
							if ( has_post_thumbnail() ) {	the_post_thumbnail('client-featured', array('class' => 'client-featured')); }  
						}

						
						echo '<div class="column two">';
								echo '<ul class="iconography horizontal light no_words">';
								echo social_link('website');
								echo social_link('facebook');
								echo social_link('twitter');
								echo '</ul>';
						echo '</div>';
					 echo '</div>';
					 
					
					endwhile;
					
					
                    // Prevent weirdness
                    wp_reset_postdata();
                    

        			echo '<div class="column two info_right horizontal">';
                	echo other_work( 'work_for_client', 'small', true, 3, 'divider horizontal on_mallow', 'Other Work for this client' , 's\'more work'  );

            		echo '</div><!-- .column,two -->';

                    endif;
					
}



// Social Links
function social_link($site) { 
						global $social_links;
						$social_links->the_meta();
						$social_links->the_field($site);
						
						while($social_links->have_fields($site))
									{
										echo '<li >';
										echo '<a href="';
										$social_links->the_value('url');
										echo '" class="'.$site.'" target="_blank" ';
										if ($site == 'website') {
											echo ' title="View Website" >&nbsp;</a>';
										} else {
											echo ' title="'.get_the_title().' on '.$site.'" >&nbsp;</a>';
										}
										echo '</li>';
									}
}

// Moonlight Links 
function moonlight_link($site) { 
						global $moonlighting_links;
						$moonlighting_links->the_meta();
						$moonlighting_links->the_field($site);
						
						while($moonlighting_links->have_fields($site))
									{
										echo '<li >';
										echo '<a href="';
										$moonlighting_links->the_value('url');
										echo '" class="'.$site.'" target="_blank"';
										if ($site == 'website') {
											echo ' title="View Website" >&nbsp;</a>';
										} else {
											echo ' title="'.get_the_title().' on '.$site.'" >&nbsp;</a>';
										}
										echo '</li>';
									}
}


function similar_work($size, $info, $number, $orderby, $service) { 

			

global $spokesimage;
$meta = $spokesimage->the_meta();	
$spokesimage->the_field('flyer');	
$spokesimage->the_field('flyer_imgurl');
$spokesimage->the_field('poster');	
$spokesimage->the_field('poster_imgurl');
$spokesimage->the_field('email');	
$spokesimage->the_field('email_imgurl');
$spokesimage->the_field('plain');	
$spokesimage->the_field('plain_imgurl');
$spokesimage->the_field('website');	
$spokesimage->the_field('website_imgurl');
$spokesimage->the_field('sermon');	
$spokesimage->the_field('sermon_imgurl');
$spokesimage->the_field('invitation');	
$spokesimage->the_field('invitation_imgurl');
$spokesimage->the_field('shirt');	
$spokesimage->the_field('shirt_imgurl');
$spokesimage->the_field('business_card');	
$spokesimage->the_field('business_card_imgurl');

$loop = new WP_Query( array( 'post_type' => 'work', 'posts_per_page' => $number, 'orderby' => $orderby, 'order' => 'ASC', 'service' => $service) );
	
			// Display related posts
			if ($loop->have_posts()) :
			
			while ( $loop->have_posts() ) : $loop->the_post();
				
				//FLYER
				//if ($spokesimage->get_the_value('flyer')) {
				//while ($spokesimage->get_the_value('flyer')) {
				echo spokesimage_url('flyer', $size, $info);
				
				
				//} }
				
				//EMAIL
				//if ($spokesimage->get_the_value('email')) {
				echo spokesimage_url('email', $size, $info);
				
				//}
				
				//WEBSITE
				//if ($spokesimage->get_the_value('website')) {
				echo web_spokesimage_url('website', $size, $info);
				
				
				//}
				
				//POSTER
				//if ($spokesimage->get_the_value('poster')) {	
				echo spokesimage_url('poster', $size, $info);
				
				
				//}
				
				//SHIRT
				//if ($spokesimage->get_the_value('shirt')) {
				echo shirt_spokesimage_url('shirt', $size, $info);
				
				
				//}
				
				//PLAIN
				//if ($spokesimage->get_the_value('plain')) {	
				echo spokesimage_url('plain', $size, $info);
				
				
				//}
				
				//INVITATION
				//if ($spokesimage->get_the_value('invitation')) {
				echo invitation_spokesimage_url('invitation', $size, $info);
				
				
				//}
				
				//SERMON
				//if ($spokesimage->get_the_value('sermon')) {
				echo spokesimage_url('sermon', $size, $info);
				
				
				//}
				
			
				//BUSINESS CARD
				//if ($spokesimage->get_the_value('business_card')) {
				echo biz_spokesimage_url('business_card', $size, $info);
				
				
	endwhile; 
			
				// Prevent weirdness
			wp_reset_postdata();
			
			endif;		
						
					
}



function work_sort($size, $info, $number, $orderby, $service, $sort) { 



// $loop = new WP_Query( array( 'post_type' => 'work', 'posts_per_page' => $number, 'orderby' => $orderby, 'order' => 'DESC', 'service' => $service) );
	
// 			// Display related posts
// 			if ($loop->have_posts()) :



global $spokesimage;
$meta = $spokesimage->the_meta();	
$spokesimage->the_field('flyer');	
$spokesimage->the_field('flyer_imgurl');
$spokesimage->the_field('poster');	
$spokesimage->the_field('poster_imgurl');
$spokesimage->the_field('email');	
$spokesimage->the_field('email_imgurl');
$spokesimage->the_field('plain');	
$spokesimage->the_field('plain_imgurl');
$spokesimage->the_field('website');	
$spokesimage->the_field('website_imgurl');
$spokesimage->the_field('sermon');	
$spokesimage->the_field('sermon_imgurl');
$spokesimage->the_field('invitation');	
$spokesimage->the_field('invitation_imgurl');
$spokesimage->the_field('shirt');	
$spokesimage->the_field('shirt_imgurl');
$spokesimage->the_field('business_card');	
$spokesimage->the_field('business_card_imgurl');
			
			// while ( $loop->have_posts() ) : $loop->the_post();
				
				//FLYER
				//if ($spokesimage->get_the_value('flyer')) {
				//while ($spokesimage->get_the_value('flyer')) {
				echo spokesimage_url('flyer', $size, $info, $sort);
				
				
				//} }
				
				//EMAIL
				//if ($spokesimage->get_the_value('email')) {
				echo spokesimage_url('email', $size, $info, $sort);
				
				//}
				
				//WEBSITE
				//if ($spokesimage->get_the_value('website')) {
				echo web_spokesimage_url('website', $size, $info, $sort);
				
				
				//}
				
				//POSTER
				//if ($spokesimage->get_the_value('poster')) {	
				echo spokesimage_url('poster', $size, $info, $sort);
				
				
				//}
				
				//SHIRT
				//if ($spokesimage->get_the_value('shirt')) {
				echo shirt_spokesimage_url('shirt', $size, $info, $sort);
				
				
				//}
				
				//PLAIN
				//if ($spokesimage->get_the_value('plain')) {	
				echo spokesimage_url('plain', $size, $info, $sort);
				
				
				//}
				
				//INVITATION
				//if ($spokesimage->get_the_value('invitation')) {
				echo invitation_spokesimage_url('invitation', $size, $info, $sort);
				
				
				//}
				
				//SERMON
				//if ($spokesimage->get_the_value('sermon')) {
				echo spokesimage_url('sermon', $size, $info, $sort);
				
				
				//}
				
			
				//BUSINESS CARD
				//if ($spokesimage->get_the_value('business_card')) {
				echo biz_spokesimage_url('business_card', $size, $info, $sort);
				
				
	// endwhile; 
			
				// Prevent weirdness
			wp_reset_postdata();
			
			// endif;		
						
					
}

/* About Graphic Design */
function about_graphic_design() { 

	echo '<h2>Graphic Design</h2>';
    echo '<a class="more left" href="'.site_url().'/work/">View Work </a>';
    echo '<br><br>	';
    
    echo '<p>Visually communicating your messages &amp; ideas with an awesome aesthetic.</p>';

    echo '<ul class="list">';
	    echo '<li>logos/branding</li>';
	    echo '<li>print</li>';
	    echo '<li>illustration</li>';
    echo '</ul>';
}



/* About Web Design*/
function about_web_design() { 

	echo '<h2>Web Design</h2>';
    echo '<a class="more left" href="'.site_url().'/work/">View Work </a>';
    echo '<br><br>	';
    
    echo '<p>Attractive, Effective and Uncomplicated, that\'s how we roll/design!</p>';

    echo '<ul class="list">';
	    echo '<li>websites</li>';
	    echo '<li>HTML email</li>';
	    echo '<li>landing pages</li>';
    echo '</ul>';
}


/* About Web Development*/
function about_web_development() { 

	echo '<h2>Web Development</h2>';
    echo '<a class="more left" href="'.site_url().'/work/">View Work </a>';
    echo '<br><br>	';
    
    echo '<p>You won\'t just love your website for it\'s looks, we\'ll make it work great too!</p>';

    echo '<ul class="list">';
	    echo '<li>Wordpress</li>';
	    echo '<li>HTMl5/CSS3</li>';
	    echo '<li>SEO</li>';
    echo '</ul>';
}




function servicebox($appearance, $service, $size, $serviceinfo, $offset) {
	//$loop = new WP_Query( array( 'post_type' => 'work', 'posts_per_page' => 1, 'orderby' => 'menu_order', 'order' => 'ASC' , 'appearances' => 'service', 'service' => 'graphic_design' ) );
$loop = new WP_Query(array( 
    'post_type' => array('work'),
    'posts_per_page' => 1,                       
    'tax_query' => array(
        'relation' => 'AND',
        array(
            'taxonomy' => 'appearances',
            'field' => 'slug',
            'terms' => $appearance
        ),
        array(
            'taxonomy' => 'service',
            'field' => 'slug',
            'terms' => $service
        )
    ),
    'orderby' => 'menu_order',  
    'order' => 'asc',
    'offset' => $offset,

) );  

if ($loop->have_posts()) :

			while ( $loop->have_posts() ) : $loop->the_post();
		
				//FLYER
				//if ($spokesimage->get_the_value('flyer')) {
				//while ($spokesimage->get_the_value('flyer')) {
				echo spokesimage_url('flyer', $size, $info, $sort, $service, $serviceinfo);
				
				
				//} }
				
				//EMAIL
				//if ($spokesimage->get_the_value('email')) {
				echo spokesimage_url('email', $size, $info, $sort, $service, $serviceinfo);
				
				//}
				
				//WEBSITE
				//if ($spokesimage->get_the_value('website')) {
				echo web_spokesimage_url('website', $size, $info, $sort, $service, $serviceinfo);
				
				
				//}
				
				//POSTER
				//if ($spokesimage->get_the_value('poster')) {	
				echo spokesimage_url('poster', $size, $info, $sort, $service, $serviceinfo);
				
				
				//}
				
				//SHIRT
				//if ($spokesimage->get_the_value('shirt')) {
				echo shirt_spokesimage_url('shirt', $size, $info, $sort, $service, $serviceinfo);
				
				
				//}
				
				//PLAIN
				//if ($spokesimage->get_the_value('plain')) {	
				echo spokesimage_url('plain', $size, $info, $sort, $service, $serviceinfo);
				
				
				//}
				
				//INVITATION
				//if ($spokesimage->get_the_value('invitation')) {
				echo invitation_spokesimage_url('invitation', $size, $info, $sort, $service, $serviceinfo);
				
				
				//}
				
				//SERMON
				//if ($spokesimage->get_the_value('sermon')) {
				echo spokesimage_url('sermon', $size, $info, $sort, $service, $serviceinfo);
				
				
				//}
				
			
				//BUSINESS CARD
				//if ($spokesimage->get_the_value('business_card')) {
				echo biz_spokesimage_url('business_card', $size, $info, $sort, $service, $serviceinfo);


				
				//$about = 'about_'.$service;
				//echo $about();
					
	endwhile; 

			endif;	
}		


function serviceinfo($service, $size) {
		echo '<p class="info '.$size.'er"><span class="mightier">';

			// PRINT
			if ($service == 'print') {
				echo 'Print';
				echo '</span>';
				echo "From business cards to posters, we design it, and ship prints to you";
			} elseif ($service == 'branding') {
				echo 'Branding';
				echo '</span>';
				echo "Be recognizable and memorable with a consistent brand";
			} elseif ($service == 'illustrations') {
				echo 'Illustrations &amp; Compositions';
				echo '</span>';
				echo "We aesthtically bring your ideas to fruition";
			} elseif ($service == 'sermon-graphic') {
				echo 'Sermon Graphics';
				echo '</span>';
				echo "Capture the interest of visitors and congregets with great graphics";
			} elseif ($service == 't-shirts') {
				echo 'T-Shirts';
				echo '</span>';
				echo "Be your own billboard by wearing your brand on your sleeve, literally";
			} elseif ($service == 'invitations') {
				echo 'Invitations &amp; Greeting Cards';
				echo '</span>';
				echo "Don't use templates, you're cooler than that";
			} elseif ($service == 'logo') {
				echo 'Logos';
				echo '</span>';
				echo "A well designed logo let's customers know you mean business";
			} elseif ($service == 'website') {
				echo 'Websites';
				echo '</span>';
				echo "From concept and design to development and launch, we got this";
			} elseif ($service == 'landing-page') {
				echo 'Landing Pages';
				echo '</span>';
				echo "Don't let your site be perpetually under construction, inform your visitors";
			} elseif ($service == 'html-email') {
				echo 'HTML Email';
				echo '</span>';
				echo "Don't just send text emails, capture and engage your customers";
											
			}
			echo "</p>";
}



function work_blog($size, $info, $number, $orderby, $service, $sort) { 


				
				//FLYER
				//if ($spokesimage->get_the_value('flyer')) {
				//while ($spokesimage->get_the_value('flyer')) {
				echo spokesimage_url('flyer', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, true);
				
				//} }
				
				//EMAIL
				//if ($spokesimage->get_the_value('email')) {
				echo spokesimage_url('email', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, true);
				
				//}
				
				//WEBSITE
				//if ($spokesimage->get_the_value('website')) {
				echo web_spokesimage_url('website', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, true);
				
				
				//}
				
				//POSTER
				//if ($spokesimage->get_the_value('poster')) {	
				echo spokesimage_url('poster', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, true);
				
				
				//}
				
				//SHIRT
				//if ($spokesimage->get_the_value('shirt')) {
				echo shirt_spokesimage_url('shirt', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, true);
				
				
				//}
				
				//PLAIN
				//if ($spokesimage->get_the_value('plain')) {	
				echo spokesimage_url('plain', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, true);
				
				
				//}
				
				//INVITATION
				//if ($spokesimage->get_the_value('invitation')) {
				echo invitation_spokesimage_url('invitation', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, true);
				
				
				//}
				
				//SERMON
				//if ($spokesimage->get_the_value('sermon')) {
				echo spokesimage_url('sermon', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, true);
				
				
				//}
				
			
				//BUSINESS CARD
				//if ($spokesimage->get_the_value('business_card')) {
				echo biz_spokesimage_url('business_card', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, true);
					
}


