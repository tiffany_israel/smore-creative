<?php

/**
 * Smore Creative Information functions
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */


//-----------INFORMATION

function theme_url() 
{ 
return "http://www.smorecreative.com/wp-content/themes/smore_creative/"; 
}



function phone() 
{ 
return "(805) 419 - YUMM"; 
}

	function phone_link() 
	{ 
	return "<li class=\"phone\"><a href=\"".phone()."\" title=\"Give us a Ring!\">".phone()."</a></li>"; 
	}
	


//-----------S'MORE SOCIAL LINKS


function email() 
{ 
return "&#121;&#117;&#109;&#64;&#115;&#109;&#111;&#114;&#101;&#99;&#114;&#56;&#118;&#46;&#99;&#111;&#109;"; 
}

	function email_link() 
	{ 
	return "<li class=\"email\"><a href=\"&#109;&#97;&#105;&#108;&#116;&#111;&#58;".email()."\" title=\"Email us!\">".email()."</a></li>"; 
	}


function facebook() 
{ 
return "http://www.fb.com/smorecr8v"; 
}

	function facebook_link() 
	{ 
	return "<li class=\"facebook\"><a href=\"".facebook()."\" title=\"Like S'more Creative on Facebook\">fb.com/smorecr8v</a></li>"; 
	}



function twitter() 
{ 
return "http://twitter.com/smore_cr8v"; 
}

	function twitter_link() 
	{ 
	return "<li class=\"twitter\"><a href=\"".twitter()."\" title=\"Follow S'more Creative on Twitter\">@smore_cr8v</a></li>"; 
	}


function linkedin() 
{ 
return "http://www.linkedin.com/company/2657058"; 
}

	function linkedin_link() 
	{ 
	return "<li class=\"linkedin\"><a href=\"".linkedin()."\" title=\"Follow S'more Creative on LinkedIn\">LinkedIn Profile</a></li>"; 
	}


function yelp() 
{ 
return "http://www.yelp.com/biz/smore-creative-oxnard"; 
}

	function yelp_link() 
	{ 
	return "<li class=\"yelp\"><a href=\"".yelp()."\" title=\"Visit S'more Creative on Yelp!\">Yelp! Profile</a></li>"; 
	}


function vcard() 
{ 
return site_url()."/smore_creative.zip"; 
}

	function vcard_link() 
	{ 
	return "<li class=\"vcard\"><a href=\"".vcard()."\" title=\"Stay in touch!\">vCard</a></li>"; 
	}
	










?>