<?php

/**
 * Smore Creative Custom Taxonomies
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */



add_action( 'init', 'create_post_taxonomies', 0 );

function create_post_taxonomies() 
{

//-----------SERVICES
  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name' => _x( 'Services', 'taxonomy general name' ),
    'singular_name' => _x( 'Service', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Services' ),
    'all_items' => __( 'All Services' ),
    'parent_item' => __( 'Parent Service' ),
    'parent_item_colon' => __( 'Parent Service:' ),
    'edit_item' => __( 'Edit Service' ), 
    'update_item' => __( 'Update Service' ),
    'add_new_item' => __( 'Add New Service' ),
    'new_item_name' => __( 'New Service Name' ),
    'menu_name' => __( 'Services' ),
  ); 	

  register_taxonomy('service',array('work'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'services' ),
	 //Other properties...
   'capabilities'=>array(
        'manage_terms' => 'manage_options',//or some other capability your clients don't have
        'edit_terms' => 'manage_options',
        'delete_terms' => 'manage_options',
        'assign_terms' =>'edit_posts'),
   //Other properties...
  ));



//-----------PROJECTS
  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name' => _x( 'Projects', 'taxonomy general name' ),
    'singular_name' => _x( 'Project', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Projects' ),
    'all_items' => __( 'All Projects' ),
    'parent_item' => __( 'Parent Project' ),
    'parent_item_colon' => __( 'Parent Project:' ),
    'edit_item' => __( 'Edit Project' ), 
    'update_item' => __( 'Update Project' ),
    'add_new_item' => __( 'Add New Project' ),
    'new_item_name' => __( 'New Project Name' ),
    'menu_name' => __( 'Projects' ),
  ); 	

  register_taxonomy('project',array('work','client'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'projects' ),
	 //Other properties...
   'capabilities'=>array(
        'manage_terms' => 'manage_options',//or some other capability your clients don't have
        'edit_terms' => 'manage_options',
        'delete_terms' => 'manage_options',
        'assign_terms' =>'edit_posts'),
   //Other properties...
  ));



//-----------CLIENTS
  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name' => _x( 'Clients', 'taxonomy general name' ),
    'singular_name' => _x( 'Client', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Clients' ),
    'all_items' => __( 'All Clients' ),
    'parent_item' => __( 'Parent Client' ),
    'parent_item_colon' => __( 'Parent Client:' ),
    'edit_item' => __( 'Edit Client' ), 
    'update_item' => __( 'Update Client' ),
    'add_new_item' => __( 'Add New Client' ),
    'new_item_name' => __( 'New Client Name' ),
    'menu_name' => __( 'Clients' ),
  ); 	

  register_taxonomy('client',array('client'), array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'clients' ),
	 //Other properties...
   'capabilities'=>array(
        'manage_terms' => 'manage_options',//or some other capability your clients don't have
        'edit_terms' => 'manage_options',
        'delete_terms' => 'manage_options',
        'assign_terms' =>'edit_posts'),
   //Other properties...
  ));



//-----------TOOLS
  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name' => _x( 'Tools', 'taxonomy general name' ),
    'singular_name' => _x( 'Tool', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Tools' ),
    'all_items' => __( 'All Tools' ),
    'parent_item' => __( 'Parent Tool' ),
    'parent_item_colon' => __( 'Parent Tool:' ),
    'edit_item' => __( 'Edit Tool' ), 
    'update_item' => __( 'Update Tool' ),
    'add_new_item' => __( 'Add New Tool' ),
    'new_item_name' => __( 'New Tool Name' ),
    'menu_name' => __( 'Tools' ),
  ); 	

  register_taxonomy('tool',array('work'), array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'tools' ),
	 //Other properties...
   'capabilities'=>array(
        'manage_terms' => 'manage_options',//or some other capability your clients don't have
        'edit_terms' => 'manage_options',
        'delete_terms' => 'manage_options',
        'assign_terms' =>'edit_posts'),
   //Other properties...
  ));



//-----------SKILLS
  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name' => _x( 'Skills', 'taxonomy general name' ),
    'singular_name' => _x( 'Skill', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Skills' ),
    'all_items' => __( 'All Skills' ),
    'parent_item' => __( 'Parent Skill' ),
    'parent_item_colon' => __( 'Parent Skill:' ),
    'edit_item' => __( 'Edit Skill' ), 
    'update_item' => __( 'Update Skill' ),
    'add_new_item' => __( 'Add New Skill' ),
    'new_item_name' => __( 'New Skill Name' ),
    'menu_name' => __( 'Skills' ),
  ); 	

  register_taxonomy('skill',array('work'), array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'skills' ),
	 //Other properties...
   'capabilities'=>array(
        'manage_terms' => 'manage_options',//or some other capability your clients don't have
        'edit_terms' => 'manage_options',
        'delete_terms' => 'manage_options',
        'assign_terms' =>'edit_posts'),
   //Other properties...
  ));



//-----------APPEARANCE
  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name' => _x( 'Appearance', 'taxonomy general name' ),
    'singular_name' => _x( 'Appearance', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Appearances' ),
    'all_items' => __( 'All Appearances' ),
    'parent_item' => __( 'Parent Appearance' ),
    'parent_item_colon' => __( 'Parent Appearance:' ),
    'edit_item' => __( 'Edit Appearance' ), 
    'update_item' => __( 'Update Appearance' ),
    'add_new_item' => __( 'Add New Appearance' ),
    'new_item_name' => __( 'New Appearance Name' ),
    'menu_name' => __( 'Appearance' ),
  ); 	

  register_taxonomy('appearances',array('work','testimonials'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'appearances' ),
	 //Other properties...
   'capabilities'=>array(
        'manage_terms' => 'manage_options',//or some other capability your clients don't have
        'edit_terms' => 'manage_options',
        'delete_terms' => 'manage_options',
        'assign_terms' =>'edit_posts'),
   //Other properties...
  ));
  
  
  
  
}