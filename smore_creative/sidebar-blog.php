<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */
?>
		<div id="secondary" class="widget-area clearfix" role="complementary">
			<?php do_action( 'before_sidebar' ); ?>

				<?php if ( is_single() ) : ?>
				<aside class="row clearfix">
					<div class="column full">
						<h1 class="divider"><span>Sharing is Caring</span></h1>
						<div class="fb-like" data-href="<?php the_permalink();?>" data-send="true" data-layout="button_count" data-width="500" data-show-faces="true" data-action="recommend"></div><br/>
						<a href="//pinterest.com/pin/create/button/?url=<?php the_permalink();?>&media=<?php echo $large_image_url[0]; ?>&description=<?php
		// ob_start();
		// the_content();
		// $old_content = ob_get_clean();
		// $new_content = strip_tags($old_content);
		// echo $new_content;
?>" data-pin-do="buttonPin" data-pin-config="beside" target="_blank"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" /></a><script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script><br/>
						<a href="https://twitter.com/share" class="twitter-share-button" data-via="smore_cr8v">Tweet</a><br/>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
					</div>
				</aside>
			<?php endif; ?>
				<aside id="categories" class="widget clearfix">	
				<?php if ( is_single() ) : ?><h1 class="divider"><span>Back to the blog</span></h1><?php endif; ?>		
					   <ul class="iconography vertical transparent">
					   		<li class="stack"><a href="<?php echo site_url();?>/blog/">All Posts</a></li>
					   		<li class="breifcase"><a href="<?php echo site_url();?>/blog/appearances/portfolio/">Completed Work</a></li>
					   		<li class="wrench"><a href="<?php echo site_url();?>/blog/appearances/wip">Work in Progress</a></li>
					   		<li class="instagram"><a href="<?php echo site_url();?>/blog/category/instagram/">Instagram</a></li>
					   		<li class="coffee"><a href="<?php echo site_url();?>/blog/category/edible_smore/">Edible S'more</a></li>
					   		<li class="heart"><a href="<?php echo site_url();?>/blog/category/edible_smore/">S'more to Love</a></li>
					   		<li class="nerd"><a href="<?php echo site_url();?>/blog/category/geekisms/">Geekisms</a></li>
					   	</ul>

				</aside>
				<aside id="archives" class="widget clearfix">
					<h1 class="divider"><span><?php _e( 'Archive', 'smore_creative' ); ?></span></h1>
					<ul class="list">
						<?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
					</ul>
				</aside>

		</div><!-- #secondary .widget-area -->
