<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */

get_header(); ?>

		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
						<div class="entry-content row" id="contact">
							
							<div class="column one">
								<h1 class="divider horizontal"><span>Get Social</span></h1><!-- .divider  -->
			            		<ul class="iconography light vertical">
			                    	<?php echo facebook_link(); ?>
			                    	<?php echo twitter_link(); ?>
			                    	<?php echo linkedin_link(); ?>
			                    	<?php echo yelp_link(); ?>
			                    </ul><!-- .iconography mid  -->

								<h1 class="divider horizontal"><span>Mailing List</span></h1><!-- .divider  -->
			                    <p>Sign up for our mailing list &amp; we'll keep you updated!</p>
					            <form autocomplete="off" action="http://smorecreative.us5.list-manage.com/subscribe/post?u=896c86a631972278c4e6c376a&amp;id=827b09082a" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="on_mallow validate" target="_blank">
					                <input type="text" name="FNAME" class="name" placeholder="name" id="mce-FNAME">
					                
					                <input type="email"  name="EMAIL" class="email" placeholder="email" id="mce-EMAIL">
					            
					                <div id="mce-responses" class="clear">
					                    <div class="response" id="mce-error-response" style="display:none"></div>
					                    <div class="response" id="mce-success-response" style="display:none"></div>
					                </div>	
					                <input type="submit" value="sign up" name="subscribe" class="button submit">
					            </form>
                    		</div><!-- .column.one -->
                    		<div class="column two">
								<h1 class="divider horizontal"><span>Send an E-Grahm</span></h1><!-- .divider  -->
								<ul class="iconography light horizontal">
				                    	<?php echo email_link(); ?>
				                    	<?php echo phone_link(); ?>
				                    	<?php echo vcard_link(); ?>
				                    </ul><!-- .iconography mid  -->

								<form  action="http://www.bluehost.com/bluemail" id="contact_page" method="post" enctype="multipart/form-data" name="ContactMe" title="Contact Me" dir="ltr" lang="en"  class="on_mallow">

									<input name="name" type="text"  id="contact_name" placeholder="name"  tabindex="1"/>

									<input name="Email" type="text" id="contact_email" placeholder="email" tabindex="2"/>

									<textarea name="message"  id="contact_message" cols="40" rows="7" placeholder="message"  tabindex="3" ></textarea>

									<div class="form_checkbox">
										<label for="inquire"  >I'm inquiring about having work done</label>
										<input type="checkbox" name="inquiring" class="checkbox" id="inquire" onclick="showMe('inquiring', this)" />  
									</div><!-- #form_checkbox -->
    
								    <div id="inquiring" style="display:none">
											<h1 class="divider horizontal"><span>Sweet! We can’t wait to work with you!</span></h1>

											<input   name="Hear" type="text" id="Hear" placeholder="How did you hear about us?" />

											<input   name="Work" type="text" id="Work" placeholder="What type of work are you interested in?" />

											<input   name="Budget" type="text" id="Budget" placeholder="Do you have a budget in mind?" />

								    </div>

									<input type="hidden" name="redirect" value="<?php echo site_url(); ?>/contact/thankyou/" /><input type="hidden" name="sendtoemail" value="<?php echo email(); ?>" />

									<div class="form_checkbox">
										<label for="mail-1"  >Subscribe to our mailing list</label>
									<input name="mailing list" id="mail-1" type="checkbox" class="checkbox" value="yes" checked /> 

									<input name="submit" type="submit" value="&#10003; &nbsp;submit" class="button submit" />
									</div><!-- #form_checkbox  -->
								</form>

							

                    		</div><!-- .column.two -->
					        
						</div><!-- .entry-content -->
					</article><!-- #post-<?php the_ID(); ?> -->


				<?php endwhile; // end of the loop. ?>

			</div><!-- #content .site-content -->
		</div><!-- #primary .content-area -->

<?php //get_sidebar(); ?>
<script>
function showMe (it, box) {
  var vis = (box.checked) ? "block" : "none";
  document.getElementById(it).style.display = vis;
}

</script>
<?php get_footer(); ?>