<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */
?>

	</div><!-- #main .site-main -->

<footer>
	<div class="footer clearfix row">
    	<div class="column one">
        	<hgroup>
            	<h1 class="divider horizontal on_mallow_dark"><span>Work</span></h1><!-- .divider  -->
                <h2><a href="#"><span class="small italic">a website for</span> Sojourn Church</a></h2>
            </hgroup>
            <span class="on_mallow_dark italic time">July 21, 2012</span>
            <p>A non-denominational church needed a website that informed regular attendees and welcomed new visitors. So we built them a dynamic Wordpress website.</p>
            <div class="computer small">
                <img src="<?php echo theme_url(); ?>assets/screen.jpg" alt="Website"  class="screen" />
                <img src="<?php echo theme_url(); ?>assets/computer_small.png" alt="Desktop computer" width="252" height="224" class="mask" />
            </div><!-- .computer .small  -->
            
            <a href="<?php echo site_url(); ?>/work"><p class="more on_mallow_dark">s'more work</p></a>
            
		</div><!-- .column.one  -->
    	<div class="column two">
        	<h1 class="divider horizontal on_mallow_dark"><span>Get Social</span></h1><!-- .divider  -->
        		<ul class="iconography mid vertical">
                	<?php echo facebook_link(); ?>
                	<?php echo twitter_link(); ?>
                	<?php echo linkedin_link(); ?>
                    <?php echo yelp_link(); ?>
                </ul><!-- .iconography mid  -->
                
               <!--  <div id="social_feed">
                	<ul>
                    	<li class="twitter"><img src="<?php echo theme_url(); ?>assets/social_pointer.png" alt="small arrow" width="20" height="20" /></li>
                    </ul> -->
                    
                   <!--  <div id="social_feed_box">
                                                    	<p class="on_orange"> -->
                                                    		<?php 
                            // $count=0;
                            // $info = json_decode(file_get_contents('https://graph.facebook.com/smorecr8v/feed/?access_token=275969055851706|mraAT4KTDmxt1BT72o10LKjW0Gw&limit=10'));


                            // if ($info) {
                            //     foreach ($info->data as $obj) {

                            // //RE-format TIME
                            // $dTime = ($obj->created_time);

                            // //RE-format Twitter link
                            // $string = $obj->message;
                            // $pattern = '/((?:f|ht)tp:\/\/[^\s]*)/i';
                            // $replacement = '<a href="$1" rel="nofollow">$1</a>';
                            // $message = preg_replace($pattern, $replacement, $string);
                            // $message = preg_replace('/\#([a-z0-9]+)/i', '<a href="http://search.twitter.com/search?q=%23$1">#$1</a>', $message);
                            // $message = preg_replace('/\@([a-z0-9]+)/i', '<a href="http://twitter.com/$1">@$1</a>', $message);
                             
                            // if ($obj->application->name =='Twitter') {
                            // $count++;
                            // if ($count == 1) {
                            //         echo $message ;
                            // echo "<time class='timeago' datetime='".$dTime."'></time>";

                            // } else {
                            // }
                            // }
                            // elseif ($obj->type =='link') { 
                             
                            // }
                            // elseif ($obj->type =='status') { 
                                 
                            // }
                            //     }   
                            // }
                            ?>
                        <!--     </p>
                    </div> --><!-- #social_feed_box  -->
               <!--  </div> --><!-- #social_feed  -->
		</div><!-- .column.two  -->
    	<div class="column three">    
        	<h1 class="divider horizontal on_mallow_dark"><span>Say Howdy</span></h1><!-- .divider  -->
        	<ul class="iconography mid horizontal">
                	<?php echo email_link(); ?>		
                	<?php echo vcard_link(); ?>				
            </ul><!-- .iconography mid  -->
            
            <?php wp_enqueue_script("uniform"); ?>
            <?php wp_enqueue_style("uniform-style"); ?>
				<form action="http://www.bluehost.com/bluemail" id="contactme" method="post" enctype="multipart/form-data" name="ContactMe" title="Contact Me" dir="ltr" lang="en" class="on_mallow_dark" >
                
                    <input onclick="select()" name="name" type="text" class="rounded_corner" id="name" placeholder="name"  />
                    
                    <input onclick="select()" name="Email" type="text" id="Email" class="rounded_corner" placeholder="email" />
                    
                    <textarea onclick="select()"  name="message"  id="message" cols="40" rows="7" class="rounded_corner" placeholder="message" ></textarea>
                    
                    <input type="hidden" name="redirect" value="<?php echo site_url(); ?>/contact/thankyou/" /><input type="hidden" name="sendtoemail" value="<?php echo email(); ?>" />
                    
					<div class="form_checkbox">

                        <label >Subscribe to our mailing list</label>
                        <input name="mailing list" type="checkbox" class="checkbox" value="yes" checked /> 
                        
                        <input name="submit" type="submit" class="rounded_corner button submit" value="&#10003; &nbsp;submit" />
					</div><!-- .form_checkbox  -->
                </form>                   
        </div><!-- .column.three  -->
	</div><!-- .footer  -->
    <div id="sub-footer">
    	<div class="footer">
			<img src="<?php echo theme_url(); ?>assets/logomark.png" alt="S'more Creative logo mark" width="339" height="194" />
            <div id="smore_punny" class="on_chocBrown rounded_corner">

                <?php 

                $loop = new WP_Query( array( 'post_type' => 'pun', 'posts_per_page' => 1, 'orderby' => 'rand') ); ?>
                <?php if ($loop->have_posts()) : ?>

                    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                        <?php
                        global $pun_meta;
                        $pun_meta->the_meta();	
                        $pun_meta->the_field('pun_meta');								
                        ?>

                        <?php while($pun_meta->have_fields('pun_meta')): ?>
                            <?php if($pun_meta->have_value('link')): ?>
                               <a href="<?php $pun_meta->the_value('link'); ?>">
                            <?php endif; ?>
                            <?php $pun_meta->the_value('pun'); ?>
                            <?php if($pun_meta->have_value('link')): ?>
                               </a>
                            <?php endif; ?>
                    
                        <?php endwhile ?>            

                    <?php endwhile; ?>

                <?php endif; ?>          

                <img src="<?php echo theme_url(); ?>assets/punny_pointer.png" alt="small arrow" width="20" height="20" />
            </div><!-- #smore_punny  -->
            <p class="on_orange">&copy; <?php echo date("Y");?> <?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?><br />
            Designed &amp; Developed by Tiffany Israel</p>
        </div> <!-- .footer -->
	</div><!-- #sub-footer -->
</footer><!-- footer  -->
        
        
</div><!-- #page .hfeed .site -->

<?php wp_footer(); ?>

<script type="text/javascript">
// Time ago twitter
 // jQuery('body').timeago();
</script>

<script type="text/javascript">

// Released under MIT license: http://www.opensource.org/licenses/mit-license.php
jQuery('[placeholder]').focus(function() {
  var input = jQuery(this);
  if (input.val() == input.attr('placeholder')) {
    input.val('');
    input.removeClass('placeholder');
  }
}).blur(function() {
  var input = jQuery(this);
  if (input.val() === '' || input.val() == input.attr('placeholder')) {
    input.addClass('placeholder');
    input.val(input.attr('placeholder'));
  }
}).blur().parents('form').submit(function() {
  jQuery(this).find('[placeholder]').each(function() {
    var input = jQuery(this);
    if (input.val() == input.attr('placeholder')) {
      input.val('');
    }
  });
});


</script>
<!-- First Child -->
<!--[if IE 7]>
<script>
var $j = jQuery.noConflict();
    $j("li:first-child")
        .css("margin-left", "0px")
        .hover(function () {
              $j(this).addClass("first");
            }, function () {
              $j(this).removeClass("first");
            });


    $j("li:last-child")
        .css("margin-right", "0px")
        .hover(function () {
              $j(this).addClass("last");
            }, function () {
              $j(this).removeClass("last");
            });

</script>

<![endif]-->


<!--[if !lt IE 9]><!-->
<script type="text/javascript">
// Tooltips (not visible on IE8 and lower)
jQuery(function() {
    jQuery(".iconography.no_words a[title]").tooltips();
});
</script>

 <!--<![endif]-->

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33190096-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>