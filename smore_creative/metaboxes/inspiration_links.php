 <div class="my_meta_control">
 
<p id="sort_warning" style="display: none; color: red;">Remember, click save or update to save your sort order</p>

 
	<p>Add links to this entry..</p>
 
	<?php while($mb->have_fields_and_multi('links')): ?>
	<?php $mb->the_group_open(); ?>

<div class="repeatable">
		<?php $mb->the_field('title'); ?>
		<p><label>Title</label><input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/></p>
 
		<?php $mb->the_field('link'); ?>
		<p><label>URL (include http://)</label><input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/></p>
 
		<?php $mb->the_field('descrip'); ?>
		<p><label>Description (optional)</label><input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/></p>
       
   
		
			<span style="text-align: right;"><a href="#" class="dodelete button">X remove link</a></span>
		</p>
 </div>
 
	<?php $mb->the_group_close(); ?>
	<?php endwhile; ?>
 
 
	<p style="margin-bottom:15px; padding-top:5px;"><a href="#" class="docopy-links button">+ add link</a>
    
	<a  href="#" class="dodelete-links button">X remove all</a></p>
    <input type="submit" class="button" name="save" value="Save" />

</div>



<script type="text/javascript">
//<![CDATA[

	jQuery(function ($)
	{
		$('#wpa_loop-links').sortable
		({
			change: function()
			{
				$('#sort_warning').show();	
			}
			});
	});
	
//]]>
</script>