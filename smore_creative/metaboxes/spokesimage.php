<?php global $wpalchemy_media_access; ?>
<div class="my_meta_control metabox" id="attachments">


	<label>Choose the type of project this is for the styling of the spokesimage</label><br/>
	
	<?php $mb->the_field('website'); ?>
	<input type="checkbox" name="<?php $mb->the_name(); ?>" value="website"<?php $mb->the_checkbox_state('website'); ?> onclick="showMe('website', this)" /> Website/Landing Page<br/>
    
    <div id="website" style="display:none">
			<?php $mb->the_field('website_imgurl'); ?>
            <?php $wpalchemy_media_access->setGroupName('aa')->setInsertButtonLabel('Insert')->setTab('type'); ?>
         
            <p>
                <?php echo $wpalchemy_media_access->getField(array('name' => $mb->get_the_name(), 'value' => $mb->get_the_value())); ?><br /><br />
                <?php echo $wpalchemy_media_access->getButton(); ?>
            </p>
    
    </div>
    
	
	<?php $mb->the_field('flyer'); ?>
	<input type="checkbox" name="<?php $mb->the_name(); ?>" value="flyer"<?php $mb->the_checkbox_state('flyer'); ?> onclick="showMe('flyer', this)" /> Flyer<br/>
    
    <div id="flyer" style="display:none">
			<?php $mb->the_field('flyer_imgurl'); ?>
            <?php $wpalchemy_media_access->setGroupName('bb')->setInsertButtonLabel('Insert')->setTab('type'); ?>
         
            <p>
                <?php echo $wpalchemy_media_access->getField(array('name' => $mb->get_the_name(), 'value' => $mb->get_the_value())); ?><br /><br />
                <?php echo $wpalchemy_media_access->getButton(); ?>
            </p>
    
    </div>
    
	
	<?php $mb->the_field('poster'); ?>
	<input type="checkbox" name="<?php $mb->the_name(); ?>" value="poster"<?php $mb->the_checkbox_state('poster'); ?> onclick="showMe('poster', this)" /> Poster<br/>
    
    <div id="poster" style="display:none">
			<?php $mb->the_field('poster_imgurl'); ?>
            <?php $wpalchemy_media_access->setGroupName('cc')->setInsertButtonLabel('Insert')->setTab('type'); ?>
         
            <p>
                <?php echo $wpalchemy_media_access->getField(array('name' => $mb->get_the_name(), 'value' => $mb->get_the_value())); ?><br /><br />
                <?php echo $wpalchemy_media_access->getButton(); ?>
            </p>
    
    </div>
    
	
	<?php $mb->the_field('shirt'); ?>
	<input type="checkbox" name="<?php $mb->the_name(); ?>" value="shirt"<?php $mb->the_checkbox_state('shirt'); ?> onclick="showMe('shirt', this)" /> T-shirt<br/>
    
    <div id="shirt" style="display:none">
			<?php $mb->the_field('shirt_imgurl'); ?>
            <?php $wpalchemy_media_access->setGroupName('dd')->setInsertButtonLabel('Insert')->setTab('type'); ?>
         
            <p><label>Upload a transparent png! AND CROP TIGHTLY TO THE DESIGN</label>
                <?php echo $wpalchemy_media_access->getField(array('name' => $mb->get_the_name(), 'value' => $mb->get_the_value())); ?><br /><br />
                <?php echo $wpalchemy_media_access->getButton(); ?>
            </p>
            
            <p><label>The hexcode value for the color background of the shirt</label>
				#<input type="text" name="<?php $metabox->the_name('shirt-color'); ?>" value="<?php $metabox->the_value('shirt-color'); ?>"/>
			</p>
    
    </div>
    
	
	<?php $mb->the_field('business_card'); ?>
	<input type="checkbox" name="<?php $mb->the_name(); ?>" value="business_card"<?php $mb->the_checkbox_state('business_card'); ?> onclick="showMe('business_card', this)" /> Business Card<br/>


    <div id="business_card" style="display:none">

            <?php $mb->the_field('biz_orientation'); ?>
            <p><strong>Orientation:</strong>
                <input type="radio" name="<?php $mb->the_name(); ?>" value="horizontal"<?php echo $mb->is_value('horizontal')?' checked="checked"':''; ?>/> horizontal
                <input type="radio" name="<?php $mb->the_name(); ?>" value="vertical"<?php echo $mb->is_value('vertical')?' checked="checked"':''; ?>/> vertical
            </p>

			<?php $mb->the_field('business_card_front_imgurl'); ?>
            <?php $wpalchemy_media_access->setGroupName('ee')->setInsertButtonLabel('Insert')->setTab('type'); ?>
         
            <p><label>FRONT</label>
                <?php echo $wpalchemy_media_access->getField(array('name' => $mb->get_the_name(), 'value' => $mb->get_the_value())); ?><br /><br />
                <?php echo $wpalchemy_media_access->getButton(); ?>
            </p>
    
			<?php $mb->the_field('business_card_back_imgurl'); ?>
            <?php $wpalchemy_media_access->setGroupName('xx')->setInsertButtonLabel('Insert')->setTab('type'); ?>
         
            <p><label>BACK</label>
                <?php echo $wpalchemy_media_access->getField(array('name' => $mb->get_the_name(), 'value' => $mb->get_the_value())); ?><br /><br />
                <?php echo $wpalchemy_media_access->getButton(); ?>
            </p>
    
    </div>
    
	
	<?php $mb->the_field('email'); ?>
	<input type="checkbox" name="<?php $mb->the_name(); ?>" value="email"<?php $mb->the_checkbox_state('email'); ?> onclick="showMe('email', this)" /> HTML Email<br/>
    
    <div id="email" style="display:none">
			<?php $mb->the_field('email_imgurl'); ?>
            <?php $wpalchemy_media_access->setGroupName('ff')->setInsertButtonLabel('Insert')->setTab('type'); ?>
         
            <p>
                <?php echo $wpalchemy_media_access->getField(array('name' => $mb->get_the_name(), 'value' => $mb->get_the_value())); ?><br /><br />
                <?php echo $wpalchemy_media_access->getButton(); ?>
            </p>
    
    </div>
    
	
	<?php $mb->the_field('plain'); ?>
	<input type="checkbox" name="<?php $mb->the_name(); ?>" value="plain"<?php $mb->the_checkbox_state('plain'); ?> onclick="showMe('plain', this)" /> Plain<br/>
    
    <div id="plain" style="display:none">
    		<label>Img should be a square dimension</label>
			<?php $mb->the_field('plain_imgurl'); ?>
            <?php $wpalchemy_media_access->setGroupName('gg')->setInsertButtonLabel('Insert')->setTab('type'); ?>
         
            <p>
                <?php echo $wpalchemy_media_access->getField(array('name' => $mb->get_the_name(), 'value' => $mb->get_the_value())); ?><br /><br />
                <?php echo $wpalchemy_media_access->getButton(); ?>
            </p>
    
    </div>
    
	
	<?php $mb->the_field('sermon'); ?>
	<input type="checkbox" name="<?php $mb->the_name(); ?>" value="sermon"<?php $mb->the_checkbox_state('sermon'); ?> onclick="showMe('sermon', this)" /> Sermon Graphic<br/>
    
    <div id="sermon" style="display:none">
			<?php $mb->the_field('sermon_imgurl'); ?>
            <?php $wpalchemy_media_access->setGroupName('hh')->setInsertButtonLabel('Insert')->setTab('type'); ?>
         
            <p>
                <?php echo $wpalchemy_media_access->getField(array('name' => $mb->get_the_name(), 'value' => $mb->get_the_value())); ?><br /><br />
                <?php echo $wpalchemy_media_access->getButton(); ?>
            </p>
    
    </div>
    
	
	<?php $mb->the_field('invitation'); ?>
	<input type="checkbox" name="<?php $mb->the_name(); ?>" value="invitation"<?php $mb->the_checkbox_state('invitation'); ?> onclick="showMe('invitation', this)" /> Invitation<br/>
    
    <div id="invitation" style="display:none">
			<?php $mb->the_field('invitation_imgurl'); ?>
            <?php $wpalchemy_media_access->setGroupName('ii')->setInsertButtonLabel('Insert')->setTab('type'); ?>
         
            <p>
                <?php echo $wpalchemy_media_access->getField(array('name' => $mb->get_the_name(), 'value' => $mb->get_the_value())); ?><br /><br />
                <?php echo $wpalchemy_media_access->getButton(); ?>
            </p>
            
            <p><label>The hexcode value for the envelope</label>
				#<input type="text" name="<?php $metabox->the_name('envelope-color'); ?>" value="<?php $metabox->the_value('envelope-color'); ?>"/>
			</p>
    
    </div>
    
 
    
 
</div>


     
     
     
<script>
function showMe (it, box) {
  var vis = (box.checked) ? "block" : "none";
  document.getElementById(it).style.display = vis;
}
</script>