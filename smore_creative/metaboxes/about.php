<div class="my_meta_control">
 
	<label>Title/Heading</label><span>(optional)</span>
 
	<p>
		<input type="text" name="<?php $metabox->the_name('title'); ?>" value="<?php $metabox->the_value('title'); ?>"/>
	
	</p>
 
	<label>About or Biographical Statement</label>
 
	<p>
		<?php $metabox->the_field('about'); ?>
		<textarea name="<?php $metabox->the_name(); ?>" rows="3"><?php $metabox->the_value(); ?></textarea>
	</p>

    
</div>