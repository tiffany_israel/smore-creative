<?php global $wpalchemy_media_access; ?>
<div class="my_meta_control" id="attachments">
<p id="sort_warning" style="display: none; color: red;">Remember, click save or update to save your sort order</p>
 
    	<p>Photos of the process of creating this project</p>
    <?php while($mb->have_fields_and_multi('docs')): ?>
    <?php $mb->the_group_open(); ?>
 
 
  <?php $mb->the_field('title'); ?>
        
        <p class="repeatable"><strong>Title:</strong>  <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>&nbsp;&nbsp;&nbsp;&nbsp;
 
 
        <?php $mb->the_field('attachurl'); ?>
        
        <?php $wpalchemy_media_access->setGroupName('img-n'. $mb->get_the_index())->setInsertButtonLabel('Insert')->setTab('type'); ?>
 
            <?php echo $wpalchemy_media_access->getButton(); ?>
            <?php echo $wpalchemy_media_access->getField(array('name' => $mb->get_the_name(), 'value' => $mb->get_the_value())); ?>
        &nbsp;&nbsp;<a href="#" class="dodelete button">X remove</a>
        </p>
 
       
 
    <?php $mb->the_group_close(); ?>
    <?php endwhile; ?>
 
    <p style="margin-bottom:15px; padding-top:5px;"><a href="#" class="docopy-docs button">+ add</a> <a href="#"  class="dodelete-docs button">X remove all</a></p>
 <input type="submit" class="button" name="save" value="Save" />

</div>


<script type="text/javascript">
//<![CDATA[

	jQuery(function ($)
	{
		$('#wpa_loop-docs').sortable
		({
			change: function()
			{
				$('#sort_warning').show();	
			}
			});
	});
	
//]]>
</script>