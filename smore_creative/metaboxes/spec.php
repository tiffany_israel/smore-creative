<?php

$pun_meta = new WPAlchemy_MetaBox(array
(
	'id' => '_pun_meta',
	'title' => 'Getting Punny with it!',
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'types' => array('pun'), // added only for pages and to custom post type specified
	'template' => get_stylesheet_directory() . '/metaboxes/pun.php',
));

$attachment_repeat = new WPAlchemy_MetaBox(array
(
	'id' => '_attachment_repeat',
	'title' => 'Upload Attachments',
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'types' => array(''), // added only for pages and to custom post type specified
	'template' => get_stylesheet_directory() . '/metaboxes/attachment_repeat.php',
));

$moonlighting_links = new WPAlchemy_MetaBox(array
(
	'id' => '_moonlighting_links',
	'title' => 'Moonlighting Links',
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'types' => array('work'), // added only for pages and to custom post type specified
	'template' => get_stylesheet_directory() . '/metaboxes/moonlighting_links.php',
));


$inspiration_links = new WPAlchemy_MetaBox(array
(
	'id' => '_inspiration_links',
	'title' => 'Inspiration Links',
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'types' => array('work'), // added only for pages and to custom post type specified
	'template' => get_stylesheet_directory() . '/metaboxes/inspiration_links.php',
));


$process_photos = new WPAlchemy_MetaBox(array
(
	'id' => '_process_photos',
	'title' => 'Process Photos',
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'types' => array('work', 'post'), // added only for pages and to custom post type specified
	'template' => get_stylesheet_directory() . '/metaboxes/process_photos.php',
));


$final_attachments = new WPAlchemy_MetaBox(array
(
	'id' => '_final_attachments',
	'title' => 'Final Work Attachments',
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'types' => array('work'), // added only for pages and to custom post type specified
	'template' => get_stylesheet_directory() . '/metaboxes/final_attachments.php',
));


$about = new WPAlchemy_MetaBox(array
(
	'id' => '_about',
	'title' => 'About',
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'types' => array('work','campers'), // added only for pages and to custom post type specified
	'template' => get_stylesheet_directory() . '/metaboxes/about.php',
));


$subtitle = new WPAlchemy_MetaBox(array
(
	'id' => '_subtitle',
	'title' => 'Subtitle',
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'types' => array('work','campers', 'post'), // added only for pages and to custom post type specified
	'template' => get_stylesheet_directory() . '/metaboxes/subtitle.php',
));


$testimonial = new WPAlchemy_MetaBox(array
(
	'id' => '_testimonial',
	'title' => 'Quote',
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'types' => array('testimonials'), // added only for pages and to custom post type specified
	'template' => get_stylesheet_directory() . '/metaboxes/testimonials.php',
));

$social_links = new WPAlchemy_MetaBox(array
(
	'id' => '_social_links',
	'title' => 'Social Links',
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'types' => array('campers', 'clients', 'post'), // added only for pages and to custom post type specified
	'template' => get_stylesheet_directory() . '/metaboxes/social_links.php',
));

$spokesimage = new WPAlchemy_MetaBox(array
(
	'id' => '_spokesimage',
	'title' => 'SpokesImage',
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'types' => array('work'), // added only for pages and to custom post type specified
	'template' => get_stylesheet_directory() . '/metaboxes/spokesimage.php',
));


$recipe_ingredients = new WPAlchemy_MetaBox(array
(
	'id' => '_recipe_ingredients',
	'title' => 'Ingredients',
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'types' => array('post'), // added only for pages and to custom post type specified
	'template' => get_stylesheet_directory() . '/metaboxes/recipe_ingredients.php',
));

$recipe_facts = new WPAlchemy_MetaBox(array
(
	'id' => '_recipe_facts',
	'title' => 'Facts',
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'types' => array('post'), // added only for pages and to custom post type specified
	'template' => get_stylesheet_directory() . '/metaboxes/recipe_facts.php',
));

$recipe_directions = new WPAlchemy_MetaBox(array
(
	'id' => '_recipe_directions',
	'title' => 'directions',
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'types' => array('post'), // added only for pages and to custom post type specified
	'template' => get_stylesheet_directory() . '/metaboxes/recipe_directions.php',
));



/* eof */