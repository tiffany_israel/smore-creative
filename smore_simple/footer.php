<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */
?>

	</div><!-- #main .site-main -->

        
</div><!-- #page .hfeed .site -->

<?php wp_footer(); ?>


<!-- First Child -->
<!--[if IE 7]>
<script>
var $j = jQuery.noConflict();
    $j("li:first-child")
        .css("margin-left", "0px")
        .hover(function () {
              $j(this).addClass("first");
            }, function () {
              $j(this).removeClass("first");
            });


    $j("li:last-child")
        .css("margin-right", "0px")
        .hover(function () {
              $j(this).addClass("last");
            }, function () {
              $j(this).removeClass("last");
            });

</script>

<![endif]-->


<!--[if !lt IE 9]><!-->
<script type="text/javascript">
// Tooltips (not visible on IE8 and lower)
jQuery(function() {
    jQuery(".iconography.no_words a[title]").tooltips();
});
</script>

 <!--<![endif]-->

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33190096-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>