<?php
/**
 *
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="entry-content">


        <div >
            <div class="column float-left">
            <!-- .column three: Process Photos if there are any -->
               <?php 
$size = 'xlarge';
        
                //FLYER
                echo spokesimage_url('flyer', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
                
                
                //EMAIL
                echo spokesimage_url('email', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
                
                
                //WEBSITE
                echo web_spokesimage_url('website', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
                
                
                
                //POSTER
                echo spokesimage_url('poster', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
                
                
                
                //SHIRT
                echo shirt_spokesimage_url('shirt', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
                
                
                
                //PLAIN
                echo spokesimage_url('plain', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
                
                
                
                //INVITATION
                echo invitation_spokesimage_url('invitation', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
                
                
                
                //SERMON
                echo spokesimage_url('sermon', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);
        
            
                //BUSINESS CARD
                echo biz_spokesimage_url('business_card', $size, $info, $sort, $service, $serviceinfo, $spokes_slide, $blog);

                ?>
            </div>
            <div class="infobox">
              <!--   <h1 class="divider horizontal on_mallow"><span>About this project</span></h1> -->

                <?php echo work_about_subtitle( 'work_for_client' ); ?>


<?php 

                    /* SKILLS */
                    echo work_about_skills($ID);
                    echo "<div class='divider blank'><br/></div>";

                    /* TOOLs */
                    echo work_about_tools($ID); ?>
                    <div class='divider blank'><br/></div>

                    <div class="row float-right">
                        <a title="Let's get started" class="button rounded_corner clearfix floater" target="_blank" href="<?php the_permalink(); ?>">Read More...</a>
                    </div>

            </div><!-- .infobox -->


        </div><!-- .row.work.about -->

        <!-- Testimonial -->
        <?php // echo work_testimonial( 'testimonial_of_work' ); ?>





    </div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
