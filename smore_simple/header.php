<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package Smore Creative
 * @since Smore Creative 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<!-- <meta name="viewport" content="width=device-width" /> RESPONSIVE -->
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	if( is_404() ) echo 'Ohh the s\'more-manity! Page not found | ';
	else wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'smore_creative' ), max( $paged, $page ) );


	?></title>
<meta name="description" content="Yummy Graphic Design and Web Services">
<meta name="keywords" content="web,graphic,design,oxnard,smore,s'more,creative"
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<meta charset="utf-8"/>

		<!-- <link rel="stylesheet" media="all" href=""/> RESPONSIVE -->
		<!-- <meta name="viewport" content="width=device-width, initial-scale=1"/> RESPONSIVE -->
		<!-- Adding "maximum-scale=1" fixes the Mobile Safari auto-zoom bug: http://filamentgroup.com/examples/iosScaleBug/  RESPONSIVE-->
		<!--[if IE 7]>
			<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/ie7-and-down.css" />
		<![endif]-->

<?php wp_enqueue_script("jquery"); ?>
<?php wp_head(); ?>


<!-- <meta property="og:title" content="Smore Creative">
<meta property="og:type" content="website">
<meta property="og:url" content="<?php $permalink = get_permalink( $id ); ?>">
<meta property="og:image" content="<?php bloginfo("template_url"); ?>/assets/open-graph.png">
<meta property="og:image:type" content="image/png">
<meta property="og:image:width" content="400">
<meta property="og:image:height" content="186">
<meta property="og:description" content="Yummy Graphic Design and Web Services">
 -->


</head>

<body <?php body_class(); ?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=275969055851706";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
<div id="page" class="hfeed site">
	<?php do_action( 'before' ); ?>
	

	<div id="main" class="site-main clearfix <?php  if ( (is_page(array('blog'))) || (is_archive()) ) { echo 'full';}  ?>">
		